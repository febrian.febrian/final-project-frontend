const colors = require('tailwindcss/colors');

/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],

  theme: {
    extend: {
      colors: {
        primary: colors.teal[700],
        secondary: colors.teal[500],
        safe: colors.sky[700],
        danger: colors.red[500],
        green: colors.green[500],
        inactive: colors.zinc[600],
        pending: colors.amber[500],
      },
    },
  },
  plugins: [],
};
