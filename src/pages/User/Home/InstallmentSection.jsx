import React, { useState } from 'react';
import InstallmentCard from '../../../components/InstallmentCard';
import PayInstallmentModal from '../../../components/PayInstallmentModal';
import { usePayInstallmentMutation } from '../../../store/slices/apiSlice';
import Section from './Section';
import Tabs from '../../../components/Tabs';

const InstallmentSection = ({
  installments = [],
  isLoading,
  onError,
  onSuccess,
}) => {
  const [installmentTabIndex, setInstallmentTabIndex] = useState(0);

  const [payingInstallment, setPayingInstallment] = useState(null);

  const [payInstallment] = usePayInstallmentMutation();

  const onPayInstallmentCardClick = (installment) => {
    setPayingInstallment(installment);
  };

  const onPayInstallmentModalClick = async (installment, voucher) => {
    try {
      let discountAmount = 0;
      let voucherCode = null;
      if (voucher) {
        discountAmount = Math.min(
          installment.amount * voucher.discount_rate,
          voucher.limit,
        );
        voucherCode = voucher.voucher_code;
      }
      const { response, error } = await payInstallment({
        installmentID: installment.id,
        amount: installment.amount + installment.penalty - discountAmount,
        voucherCode: voucherCode,
      });

      if (error) {
        onError(error.data.endpoint_code);
        setPayingInstallment(null);
      }

      if (response) {
        onSuccess();
        setPayingInstallment(null);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onCancelPaymentModalClick = () => {
    setPayingInstallment(null);
  };

  const onInstallmentTabChange = (index) => {
    setInstallmentTabIndex(index);
  };

  const getUnpaidInstallments = () => {
    return installments.filter((installment) => installment.payment === null);
  };

  const getPaidInstallments = () => {
    return installments.filter((installment) => installment.payment !== null);
  };

  const renderInstallments = (givenInstallments) => {
    return givenInstallments.map((installment) => {
      return (
        <InstallmentCard
          installment={installment}
          onPayClick={onPayInstallmentCardClick}
        />
      );
    });
  };

  const renderOnLoading = () => {
    return Array(3)
      .fill()
      .map(() => {
        return <InstallmentCard isLoading />;
      });
  };

  const render = () => {
    if (isLoading) {
      return <div className="grid grid-cols-4 gap-4">{renderOnLoading()}</div>;
    }
    if (installments.length > 0) {
      return (
        <Tabs
          index={installmentTabIndex}
          headers={['Unpaid', 'Paid']}
          onTabChange={onInstallmentTabChange}
        >
          <div className="grid grid-cols-4 gap-4">
            {renderInstallments(getUnpaidInstallments())}
          </div>
          <div className="grid grid-cols-4 gap-4">
            {renderInstallments(getPaidInstallments())}
          </div>
        </Tabs>
      );
    }
    return (
      <div className="h-[150px] flex justify-center items-center">
        <p>No active installments yet</p>
      </div>
    );
  };

  return (
    <Section header="Active Installments">
      <PayInstallmentModal
        installment={payingInstallment}
        onCancel={onCancelPaymentModalClick}
        onPay={onPayInstallmentModalClick}
      />
      {render()}
    </Section>
  );
};

export default InstallmentSection;
