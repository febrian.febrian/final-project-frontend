import React, { useState } from 'react';
import LoanCard from '../../../components/LoanCard';
import Tabs from '../../../components/Tabs';

const LoanSection = ({ loans = [], isLoading }) => {
  const [tabIndex, setTabIndex] = useState(0);

  const onTabChange = (index) => {
    setTabIndex(index);
  };

  const renderOnLoading = () => {
    return Array(3)
      .fill()
      .map(() => {
        return <LoanCard isLoading />;
      });
  };

  const getLoansByStatus = (status) => {
    return loans.filter((loan) => loan.status === status);
  };

  const renderTabData = (givenLoans) => {
    return givenLoans.map((loan) => <LoanCard loan={loan} />);
  };

  return (
    <div>
      <h1 className="text-4xl font-bold text-primary mb-1">Loans</h1>
      <Tabs
        headers={['Active', 'Pending', 'Rejected', 'History']}
        index={tabIndex}
        onTabChange={onTabChange}
      >
        <div className="grid grid-cols-4 gap-4">
          {isLoading
            ? renderOnLoading
            : renderTabData(getLoansByStatus('active'))}
        </div>
        <div className="grid grid-cols-4 gap-4">
          {isLoading
            ? renderOnLoading
            : renderTabData(getLoansByStatus('pending'))}
        </div>
        <div className="grid grid-cols-4 gap-4">
          {isLoading
            ? renderOnLoading
            : renderTabData(getLoansByStatus('rejected'))}
        </div>
        <div className="grid grid-cols-4 gap-4">
          {isLoading
            ? renderOnLoading
            : renderTabData(getLoansByStatus('done'))}
        </div>
      </Tabs>
    </div>
  );
};

export default LoanSection;
