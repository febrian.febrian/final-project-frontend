import React, { useEffect, useState } from 'react';
import ContractBanner from '../../../components/ContractBanner';
import Page from '../../../components/Page';
import translate, {
  getProfileMap,
  payInstallmentMap,
} from '../../../utils/Translator';
import { useGetProfileQuery } from '../../../store/slices/apiSlice';

import ErrorBanner from '../../../components/ErrorBanner';
import LoanSection from './LoanSection';
import InstallmentSection from './InstallmentSection';
import FullColorButton from '../../../components/FullColorButton';
import RequestLoanModal from '../../../components/RequestLoanModal';
import ProfileSection from './ProfileSection';

const Home = () => {
  const {
    data: response,
    isFetching,
    error: fetchError,
  } = useGetProfileQuery();

  const [showRequestModal, setShowRequestModal] = useState(false);
  const [error, setError] = useState(null);

  const showError = (err) => {
    setError(err);
  };

  useEffect(() => {
    if (fetchError) {
      console.log(fetchError);
      showError(translate(getProfileMap, fetchError.data.endpoint_code));
    }
  }, [fetchError]);

  const onErrorConfirmContract = (endpointCode) => {
    showError(translate({}, endpointCode));
  };

  const openRequestModal = () => {
    setShowRequestModal(true);
  };

  const closeRequestModal = () => {
    setShowRequestModal(false);
  };

  const onErrorPayingInstallment = (endpointCode) => {
    showError(translate(payInstallmentMap, endpointCode));
  };

  return (
    <Page>
      {error && <ErrorBanner title={error.title} message={error.message} />}

      <ProfileSection
        user={response?.user}
        installments={response?.active_installments}
        loans={response?.loans}
        isLoading={isFetching}
      />

      <div className="mt-5 flex justify-center">
        <ContractBanner
          status={response?.user.contract_status_enum}
          isLoading={isFetching}
          onError={onErrorConfirmContract}
        />
      </div>

      <div className="my-8">
        <div className="flex justify-end">
          {response?.user.contract_status_enum === 5 && (
            <FullColorButton
              backgroundColor="bg-secondary"
              onClick={openRequestModal}
            >
              + Request New Loan
            </FullColorButton>
          )}

          <RequestLoanModal
            show={showRequestModal}
            onCancel={closeRequestModal}
            onSuccess={closeRequestModal}
          />
        </div>
        <InstallmentSection
          installments={response?.active_installments}
          isLoading={isFetching}
          onError={onErrorPayingInstallment}
        />
      </div>

      <div className="my-8">
        <LoanSection loans={response?.loans} isLoading={isFetching} />
      </div>
    </Page>
  );
};

export default Home;
