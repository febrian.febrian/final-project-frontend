import React from 'react';
import Loading from '../../../components/Loading';

const ProfileSection = ({ user, loans, installments, isLoading }) => {
  const renderOnLoading = () => {
    return (
      <div>
        <h1>
          Hello,
          <Loading />
        </h1>
        <p>
          Available credit:
          <Loading />
        </p>
        <p>
          Credit limit:
          <Loading />
        </p>
      </div>
    );
  };

  const getActiveLoansCount = () => {
    return loans.filter((loan) => loan.status === 'active').length;
  };

  const getUnpaidInstallments = () => {
    return installments.filter((installment) => {
      return installment.payment === null;
    });
  };

  const getTotalAmountForUnpaidInstallments = () => {
    const totalAmounts = getUnpaidInstallments().map((installment) => {
      return installment.amount + installment.penalty;
    });

    if (totalAmounts.length > 0) {
      return totalAmounts.reduce((sum, now) => sum + now);
    }
    return 0;
  };

  return isLoading ? (
    renderOnLoading()
  ) : (
    <div>
      <div className="flex w-full">
        <p className="text-3xl font-bold text-primary">Hello,</p>
        <span className="text-3xl font-bold text-secondary">
          {!isLoading ? user.name : <Loading />}
        </span>
      </div>
      <div className="my-2">
        <p className="w-[35%] flex justify-between">
          <p className="font-bold text-primary ">Available credit</p>
          <span>
            {!isLoading ? (
              (
                user.credit_status.limit - user.credit_status.usage
              ).toLocaleString('id')
            ) : (
              <Loading />
            )}
          </span>
        </p>
        <p className="w-[35%] flex justify-between">
          <p className="font-bold text-primary">Credit limit</p>
          <span>
            {!isLoading ? (
              user.credit_status.limit.toLocaleString('id')
            ) : (
              <Loading />
            )}
          </span>
        </p>
      </div>

      <div>
        <p className="w-[35%] flex justify-between">
          <p className="font-bold text-primary">Active Loans</p>
          <span className="font-bold text-secondary">
            {getActiveLoansCount()}
          </span>
        </p>
        <p className="w-[35%] flex justify-between">
          <p className="text-primary font-bold">Unpaid installments</p>
          <span className="font-bold text-danger">
            {getUnpaidInstallments().length}
          </span>
        </p>
        <p className="w-[35%] flex justify-between">
          <p className="text-primary font-bold">Total amount due</p>
          <span className="font-bold text-danger">
            {getTotalAmountForUnpaidInstallments().toLocaleString('id')}
          </span>
        </p>
      </div>
    </div>
  );
};

export default ProfileSection;
