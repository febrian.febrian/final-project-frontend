import React from 'react';

const Section = ({ header, children }) => {
  return (
    <div className="min-h-[300px]">
      <h1 className="text-4xl font-bold text-primary mb-1">{header}</h1>
      {children}
    </div>
  );
};

export default Section;
