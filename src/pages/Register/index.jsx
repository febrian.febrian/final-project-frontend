import React, { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
import ErrorBanner from '../../components/ErrorBanner';
import Page from '../../components/Page';
import TextValidationError from '../../components/TextValidationError';
import { useRegisterMutation } from '../../store/slices/apiSlice';
import translate, { registerMap } from '../../utils/Translator';

const registerSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  email: Yup.string().required('Required').email('Invalid email'),
  password: Yup.string()
    .required('Required')
    .min(6, 'Min. 6 characters required'),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password'), null],
    'Confirm password does not match',
  ),
});

const Register = () => {
  const [error, setError] = useState(null);

  const [register] = useRegisterMutation();

  const navigate = useNavigate();

  const onRegisterSubmit = async (values) => {
    setError(null);

    try {
      const response = await register({
        name: values.name,
        email: values.email,
        password: values.password,
      });

      if (response.error) {
        setError(translate(registerMap, response.error.data.endpoint_code));
      } else {
        navigate('/login');
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Page>
      <div className="flex flex-col justify-center min-h-[80vh]">
        <div className="w-full flex flex-col items-center">
          <Formik
            validationSchema={registerSchema}
            validateOnBlur
            onSubmit={onRegisterSubmit}
            initialValues={{
              name: '',
              email: '',
              password: '',
            }}
          >
            {({ errors, touched }) => (
              <Form className="flex flex-col w-full justify-center items-center w-1/4">
                <div className="w-full">
                  <Field
                    id="name"
                    name="name"
                    placeholder="Your full name"
                    className="my-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full py-3 px-4"
                  />
                  {touched.name && errors.name && (
                    <TextValidationError text={errors.name} />
                  )}
                </div>
                <div className="w-full">
                  <Field
                    id="email"
                    name="email"
                    placeholder="Your email"
                    className="my-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full py-3 px-4"
                  />
                  {touched.email && errors.email && (
                    <TextValidationError text={errors.email} />
                  )}
                </div>
                <div className="w-full">
                  <Field
                    type="password"
                    id="password"
                    name="password"
                    placeholder="Password"
                    className="my-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full py-3 px-4"
                  />
                  {touched.password && errors.password && (
                    <TextValidationError text={errors.password} />
                  )}
                </div>
                <div className="w-full">
                  <Field
                    type="password"
                    id="confirmPassword"
                    name="confirmPassword"
                    placeholder="Password"
                    className="my-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full py-3 px-4"
                  />
                  {touched.confirmPassword && errors.confirmPassword && (
                    <TextValidationError text={errors.confirmPassword} />
                  )}
                </div>
                {error && (
                  <ErrorBanner title={error.title} message={error.message} />
                )}
                <button
                  type="submit"
                  className="h-[50px] my-2 bg-secondary rounded-3xl text-white w-full"
                >
                  Register
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </Page>
  );
};

export default Register;
