import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import ErrorBanner from '../../components/ErrorBanner';
import Page from '../../components/Page';
import Table from '../../components/Table';
import { useGetUserByIDQuery } from '../../store/slices/apiSlice';
import translate, { getUserMap } from '../../utils/Translator';
import UserDetailArea from './area';
import UserInfo from './UserInfo';

const UserDetail = () => {
  const { userID } = useParams();
  const {
    data: response,
    isFetching,
    error: fetchError,
  } = useGetUserByIDQuery(userID);
  const [error, setError] = useState(null);

  const onAPICallError = (err) => {
    setError(err);
  };

  useEffect(() => {
    if (fetchError) {
      onAPICallError(translate(getUserMap, fetchError.data.endpoint_code));
    }
  }, [fetchError]);

  return (
    <Page width="w-4/5">
      {error && <ErrorBanner title={error.title} message={error.message} />}
      <div>
        <h1 className="text-emerald-600 font-extrabold text-2xl">
          User Detail
        </h1>
      </div>
      <UserInfo user={response?.user ?? null} onError={onAPICallError} />
      <UserDetailArea title="Loans">
        <Table
          headers={['Loan ID', 'Purpose', 'Tenor', 'Amount', 'Status']}
          isLoading={isFetching}
          data={response?.loans ?? []}
          renderRow={(loan) => {
            return (
              <>
                <td className="py-4 px-6">{loan.id}</td>
                <td className="py-4 px-6">{loan.purpose}</td>
                <td className="py-4 px-6">{loan.tenor}</td>
                <td className="py-4 px-6">{loan.amount}</td>
                <td className="py-4 px-6">{loan.status}</td>
              </>
            );
          }}
        />
      </UserDetailArea>
      <UserDetailArea title="Payments">
        <Table
          headers={['Loan ID', 'Purpose', 'Tenor', 'Amount', 'Status']}
          isLoading={isFetching}
          data={response?.payments ?? []}
          renderRow={(payment) => {
            <td className="py-4 px-6">{payment.id}</td>;
          }}
        />
      </UserDetailArea>
    </Page>
  );
};

export default UserDetail;
