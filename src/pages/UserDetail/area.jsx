import React from 'react';
import PropTypes from 'prop-types';

const UserDetailArea = ({ title, children }) => {
  return (
    <div className="min-h-[30vh] w-[300px]">
      <h2 className="text-emerald-600 font-bold text-lg">{title}</h2>
      {children}
    </div>
  );
};

UserDetailArea.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default UserDetailArea;
