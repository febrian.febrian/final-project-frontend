import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Editable from '../../components/Editable';
import {
  useUpdateContractStatusMutation,
  useUpdateCreditLimitMutation,
} from '../../store/slices/apiSlice';
import UserDetailArea from './area';
import UserInfoEntry from './UserInfoEntry';
import translate, {
  updateContractStatusMap,
  updateCreditLimitMap,
} from '../../utils/Translator';
import Loading from '../../components/Loading';
import { FormatDate } from '../../utils/TimeFormat';

const UserInfo = ({ user, onError }) => {
  const [selectedContractStatus, setSelectedContractStatus] = useState(1);
  const [creditLimit, setCreditLimit] = useState(0);
  const [creditHealth, setCreditHealth] = useState('');

  useEffect(() => {
    if (user) {
      setSelectedContractStatus(user.contract_status_enum);
      setCreditHealth(user.credit_status.health);
      setCreditLimit(user.credit_status.limit);
    }
  }, [user]);

  const [updateCreditLimit] = useUpdateCreditLimitMutation();
  const [updateContractStatus] = useUpdateContractStatusMutation();

  useEffect(() => {
    if (user) {
      setSelectedContractStatus(user.contract_status_enum);
      setCreditLimit(user.credit_status.limit);
      setCreditHealth(user.credit_status.health);
    }
  }, []);

  const onContractStatusSave = async () => {
    try {
      const updateResponse = await updateContractStatus({
        userID: user.id,
        state: parseInt(selectedContractStatus, 10),
      });
      if (updateResponse.error) {
        onError(
          translate(
            updateContractStatusMap,
            updateResponse.error.data.endpoint_code,
          ),
        );
        setSelectedContractStatus(user.contract_status_enum);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onContractStatusChange = (e) => {
    setSelectedContractStatus(e.target.value);
  };

  const onContractStatusCancel = () => {
    setSelectedContractStatus(user.contract_status_enum);
  };

  const onCreditLimitChange = (e) => {
    setCreditLimit(e.target.value);
  };

  const onCreditLimitCancel = () => {
    setCreditLimit(user.credit_status.limit);
  };

  const onCreditLimitSave = async () => {
    try {
      const updateCreditResponse = await updateCreditLimit({
        creditLimit: parseInt(creditLimit, 10),
        userID: user.id,
      });

      if (updateCreditResponse.error) {
        onError(
          translate(
            updateCreditLimitMap,
            updateCreditResponse.error.data.endpoint_code,
          ),
        );
        setCreditLimit(user.credit_status.limit);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const renderContractStatusOptions = () => {
    const contractStatusMap = {
      1: 'No contract has been issued yet',
      2: 'Contract is sent to expedition',
      3: 'Contract is on delivery',
      4: 'Contract is received by user',
    };

    return Array.from('1234').map((value) => {
      return (
        <option value={value} disabled={value < user.contract_status_enum}>
          {contractStatusMap[value]}
        </option>
      );
    });
  };

  return (
    <UserDetailArea title="User info">
      <UserInfoEntry entryKey="User ID">
        <p>{user ? user.id : ''}</p>
      </UserInfoEntry>
      <UserInfoEntry entryKey="Name">
        <p>{user ? user.name : ''}</p>
      </UserInfoEntry>
      <UserInfoEntry entryKey="Email">
        <p>{user ? user.email : ''}</p>
      </UserInfoEntry>
      <UserInfoEntry entryKey="Join date">
        <p>{user ? FormatDate(user.join_date) : ''}</p>
      </UserInfoEntry>
      <UserInfoEntry entryKey="Contract Status">
        {user ? (
          <Editable
            onCancel={onContractStatusCancel}
            onSave={onContractStatusSave}
          >
            <select
              value={selectedContractStatus}
              onChange={onContractStatusChange}
            >
              {renderContractStatusOptions()}
              <option disabled value={5}>
                Contract is confirmed
              </option>
            </select>
          </Editable>
        ) : (
          <Loading />
        )}
      </UserInfoEntry>
      <UserInfoEntry entryKey="Credit Status">
        <p>{creditHealth}</p>
      </UserInfoEntry>
      <UserInfoEntry entryKey="Credit Limit">
        <Editable onCancel={onCreditLimitCancel} onSave={onCreditLimitSave}>
          <input
            type="number"
            value={creditLimit}
            onChange={onCreditLimitChange}
          />
        </Editable>
      </UserInfoEntry>
    </UserDetailArea>
  );
};

UserInfo.propTypes = {
  id: PropTypes.number.isRequired,
};

export default UserInfo;
