import React from 'react';
import PropTypes from 'prop-types';

const UserInfoEntry = ({ entryKey, children }) => {
  return (
    <div className="grid grid-cols-2 py-1">
      <p>{entryKey}</p>
      <div>{children}</div>
    </div>
  );
};

UserInfoEntry.propTypes = {
  entryKey: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default UserInfoEntry;
