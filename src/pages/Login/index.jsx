import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ErrorBanner from '../../components/ErrorBanner';
import Page from '../../components/Page';
import { useLoginMutation } from '../../store/slices/apiSlice';

import translate, { loginMap } from '../../utils/Translator';
import TextValidationError from '../../components/TextValidationError';

const loginSchema = Yup.object().shape({
  email: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

const Login = () => {
  const navigate = useNavigate();

  const [error, setError] = useState();

  const [login] = useLoginMutation();

  const onLoginBtnClick = async (values) => {
    setError(null);

    try {
      const response = await login({
        email: values.email,
        password: values.password,
      });

      if (response.error) {
        setError(translate(loginMap, response.error.data.endpoint_code));
      } else {
        localStorage.setItem('token', response.data.token);
        if (response.data.user.role === 'admin') {
          navigate('/admin', { replace: true });
        } else {
          navigate('/', { replace: true });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Page>
      <div className="flex flex-col justify-center min-h-[80vh]">
        <div className="w-full flex flex-col items-center">
          <Formik
            validationSchema={loginSchema}
            validateOnBlur
            onSubmit={onLoginBtnClick}
            initialValues={{
              email: '',
              password: '',
            }}
          >
            {({ errors, touched }) => (
              <Form className="flex flex-col w-full justify-center items-center w-1/4">
                <div className="w-full">
                  <Field
                    id="email"
                    name="email"
                    placeholder="Email"
                    className="my-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full py-3 px-4"
                  />
                  {touched.email && errors.email && (
                    <TextValidationError text={errors.email} />
                  )}
                </div>
                <div className="w-full">
                  <Field
                    type="password"
                    id="password"
                    name="password"
                    placeholder="Password"
                    className="my-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full py-3 px-4"
                  />
                  {touched.password && errors.password && (
                    <TextValidationError text={errors.password} />
                  )}
                </div>
                {error && (
                  <ErrorBanner title={error.title} message={error.message} />
                )}
                <button
                  type="submit"
                  className="h-[50px] my-2 bg-secondary text-white rounded-3xl w-full"
                >
                  Log in
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </Page>
  );
};

export default Login;
