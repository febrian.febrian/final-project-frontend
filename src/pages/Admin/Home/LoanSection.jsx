import React, { useCallback, useState } from 'react';
import LoanTable from '../../../components/LoanTable/LoanTable';
import Tabs from '../../../components/Tabs';

import {
  useLazyGetActiveLoansQuery,
  useLazyGetHistoryLoansQuery,
  useLazyGetPendingLoansQuery,
} from '../../../store/slices/apiSlice';

const LoanSection = () => {
  const [getActiveLoans] = useLazyGetActiveLoansQuery();
  const [getPendingLoans] = useLazyGetPendingLoansQuery();
  const [getHistoryLoans] = useLazyGetHistoryLoansQuery();

  const [activeLoans, setActiveLoans] = useState(null);
  const [pendingLoans, setPendingLoans] = useState(null);
  const [historyLoans, setHistoryLoans] = useState(null);

  const [tabIndex, setTabIndex] = useState(0);

  const onLoanTabChanged = (index) => {
    setTabIndex(index);
  };

  const onFilterActiveLoansChanged = useCallback(async (filter) => {
    const { data } = await getActiveLoans(filter);
    setActiveLoans(data);
  }, []);

  const onFilterPendingLoansChanged = useCallback(async (filter) => {
    const { data } = await getPendingLoans(filter);
    setPendingLoans(data);
  }, []);

  const onFilterHistoryLoansChanged = useCallback(async (filter) => {
    const { data } = await getHistoryLoans(filter);
    setHistoryLoans(data);
  }, []);
  return (
    <div>
      <h1 className="text-2xl font-bold">Loans</h1>
      <div>
        <Tabs
          headers={['Active Loans', 'Pending Requests', 'History']}
          index={tabIndex}
          onTabChange={onLoanTabChanged}
        >
          <LoanTable
            response={activeLoans}
            onFilterChange={onFilterActiveLoansChanged}
          />
          <LoanTable
            response={pendingLoans}
            onFilterChange={onFilterPendingLoansChanged}
          />
          <LoanTable
            response={historyLoans}
            onFilterChange={onFilterHistoryLoansChanged}
          />
        </Tabs>
      </div>
    </div>
  );
};

export default LoanSection;
