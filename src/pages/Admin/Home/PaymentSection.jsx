import React, { useState } from 'react';
import PaymentTable from '../../../components/PaymentTable';
import { useLazyGetPaymentsQuery } from '../../../store/slices/apiSlice';

const PaymentSection = () => {
  const [payments, setPayments] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [loading, setLoading] = useState(true);
  const [getPayments] = useLazyGetPaymentsQuery();

  const onTableFilterChanged = async (filter) => {
    try {
      setLoading(true);
      const { data: response, error } = await getPayments(filter);
      if (response) {
        setPayments(response.payments);
        setLoading(false);
        setTotalPage(response.total_page);
      }
      if (error) {
        console.log(error.data.endpoint_code);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <h1 className="text-2xl font-bold">Payments</h1>
      <div>
        <PaymentTable
          onFilterChanged={onTableFilterChanged}
          isLoading={loading}
          pageCount={totalPage}
          payments={payments}
        />
      </div>
    </div>
  );
};

export default PaymentSection;
