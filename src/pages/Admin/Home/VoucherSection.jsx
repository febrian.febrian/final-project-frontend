import React, { useState } from 'react';
import CreateVoucherModal from '../../../components/CreateVoucherModal';
import VoucherTable from '../../../components/VoucherTable';
import { useLazyGetVouchersQuery } from '../../../store/slices/apiSlice';
import translate from '../../../utils/Translator';
import FullColorButton from '../../../components/FullColorButton';

const VoucherSection = () => {
  const [getVouchers] = useLazyGetVouchersQuery();

  const [showModal, setShowModal] = useState(false);

  const closeModal = () => {
    setShowModal(false);
  };

  const openModal = () => {
    setShowModal(true);
  };

  const [vouchers, setVouchers] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [loading, setLoading] = useState(true);

  const [error, setError] = useState(null);

  const onTableFilterChanged = async (filter) => {
    try {
      setError(null);
      const { data: response, error: fetchErr } = await getVouchers(filter);
      if (response) {
        setVouchers(response.data.vouchers);
        setLoading(false);
        setTotalPage(response.data.total_page);
      }

      if (error) {
        setLoading(false);
        setError(translate({}, fetchErr.data.endpoint_code));
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <CreateVoucherModal
        show={showModal}
        onSuccess={closeModal}
        onCancel={closeModal}
      />
      <h1 className="text-2xl font-bold">Vouchers</h1>
      <FullColorButton
        backgroundColor="bg-secondary"
        className="my-3"
        onClick={openModal}
      >
        + New Voucher
      </FullColorButton>
      <div>
        <VoucherTable
          vouchers={vouchers}
          pageCount={totalPage}
          isLoading={loading}
          onFilterChanged={onTableFilterChanged}
        />
      </div>
    </div>
  );
};

export default VoucherSection;
