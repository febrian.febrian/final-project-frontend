import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ErrorBanner from '../../../components/ErrorBanner';
import UserTable from '../../../components/UserTable';
import { useLazyGetUsersQuery } from '../../../store/slices/apiSlice';
import translate from '../../../utils/Translator';

const UserSection = () => {
  const navigate = useNavigate();

  const [getUsers] = useLazyGetUsersQuery();

  const [users, setUsers] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const fetchUsers = async (filter) => {
    try {
      setLoading(true);
      const { data: response, error: responseError } = await getUsers(filter);

      if (response) {
        setUsers(response.users);
        setTotalPage(response.total_page);
        setLoading(false);
        return;
      }

      if (responseError) {
        setError(translate({}, responseError.data.endpoint_code));
        setUsers([]);
        setLoading(false);
        return;
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onUserClicked = (user) => {
    navigate(`/admin/users/${user.id}`);
  };

  const onFilterChanged = async (filter) => {
    await fetchUsers(filter);
  };

  return (
    <div className="min-h-[300px]">
      <h1 className="text-2xl font-bold">Users</h1>
      <div>
        {error && <ErrorBanner title={error.title} message={error.message} />}
        <UserTable
          users={users}
          isLoading={loading}
          pageCount={totalPage}
          onFilterChanged={onFilterChanged}
          onUserClick={onUserClicked}
        />
      </div>
    </div>
  );
};

export default UserSection;
