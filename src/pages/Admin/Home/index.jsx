import React from 'react';

import Page from '../../../components/Page';
import VoucherSection from './VoucherSection';
import PaymentSection from './PaymentSection';
import LoanSection from './LoanSection';
import UserSection from './UserSection';

const Dashboard = () => {
  return (
    <Page>
      <section id="loans">
        <LoanSection />
      </section>
      <section id="users">
        <UserSection />
      </section>
      <section id="payments">
        <PaymentSection />
      </section>
      <section id="vouchers">
        <VoucherSection />
      </section>
    </Page>
  );
};

export default Dashboard;
