import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import Page from '../../components/Page';
import LoanInfo from '../../components/LoanInfo';
import { useGetLoanDetailQuery } from '../../store/slices/apiSlice';
import Loading from '../../components/Loading';
import InstallmentTable from '../../components/InstallmentTable';
import ErrorBanner from '../../components/ErrorBanner';
import translate from '../../utils/Translator';

const LoanDetail = () => {
  const { loanID } = useParams();
  const { data: response, isFetching, refetch } = useGetLoanDetailQuery(loanID);
  const [error, setError] = useState(null);

  const getLoanInfo = () => {
    if (isFetching) {
      return <Loading />;
    }

    const { loan } = response;

    const onUpdateLoanError = (errorMap, endpointCode) => {
      setError(translate(errorMap, endpointCode));
    };

    const onSuccessRefetch = () => {
      refetch();
    };

    return (
      <LoanInfo
        id={loan.id}
        purpose={loan.purpose}
        description={loan.description}
        tenor={loan.tenor}
        amount={loan.amount}
        tenorRate={loan.tenor_rate}
        status={loan.status}
        totalAmount={loan.total_amount}
        userID={loan.user.id}
        userName={loan.user.name}
        userEmail={loan.user.email}
        requestDate={loan.request_date}
        monthlyDueDate={loan.monthly_due_date}
        isApproved={loan.status !== 'pending'}
        approverID={loan.approved_by?.id}
        approverName={loan.approved_by?.name}
        approvalDate={loan.approved_by === null ? '-' : loan.approval_date}
        onError={onUpdateLoanError}
        onSuccess={onSuccessRefetch}
      />
    );
  };

  const renderInstallmentsTable = () => {
    if (isFetching) {
      return <Loading />;
    }

    const { loan } = response;
    if (loan.status !== 'active') {
      return <div>Loan is not active</div>;
    }

    return <InstallmentTable installments={loan.installments} />;
  };

  return (
    <Page>
      {error && <ErrorBanner title={error.title} message={error.message} />}
      <div>
        <h1>Loan Info</h1>
        {getLoanInfo()}
      </div>
      <div>
        <h1>Installments</h1>
        {renderInstallmentsTable()}
      </div>
    </Page>
  );
};

export default LoanDetail;
