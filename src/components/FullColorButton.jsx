import React from 'react';
import PropTypes from 'prop-types';

const FullColorButton = ({ backgroundColor, onClick, children, className }) => {
  return (
    <button
      type="button"
      className={`${backgroundColor} text-white font-bold py-2 px-4 rounded-xl ${className}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

FullColorButton.defaultProps = {
  onClick: () => {},
  className: '',
};

FullColorButton.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default FullColorButton;
