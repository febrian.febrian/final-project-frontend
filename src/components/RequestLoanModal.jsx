import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal from './Modal';
import ErrorBanner from './ErrorBanner';
import translate, { requestLoanMap } from '../utils/Translator';
import RequestLoanForm from './RequestLoanForm';

const RequestLoanModal = ({ show, onCancel, onSuccess }) => {
  const [error, setError] = useState(null);

  const onRequestLoanFail = (endpointCode) => {
    setError(translate(requestLoanMap, endpointCode));
  };

  return (
    <Modal
      onClose={onCancel}
      show={show}
      title="Request Loan"
      body={
        <div>
          {error && <ErrorBanner title={error.title} message={error.message} />}
          <RequestLoanForm
            onClose={onCancel}
            onError={onRequestLoanFail}
            onSuccess={onSuccess}
          />
        </div>
      }
    />
  );
};

RequestLoanModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default RequestLoanModal;
