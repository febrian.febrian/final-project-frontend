import React from 'react';
import Loading from './Loading';

const Voucher = ({ voucher, isLoading, onChoose }) => {
  const renderOnLoading = () => {
    return (
      <>
        <div>
          <Loading />
        </div>
        <div>
          <Loading />
        </div>
      </>
    );
  };

  const render = () => {
    const message = `${
      voucher.discount_rate * 100
    }% off (max. ${voucher.limit.toLocaleString('id')})`;

    return (
      <>
        <div>{message}</div>
        <div>{`Code: ${voucher.voucher_code}`}</div>
      </>
    );
  };

  return (
    <button
      type="button"
      className="p-3 border-2 border-secondary rounded-3xl hover:bg-gray-300"
      onClick={() => {
        onChoose(voucher);
      }}
    >
      {isLoading ? renderOnLoading() : render()}
    </button>
  );
};

export default Voucher;
