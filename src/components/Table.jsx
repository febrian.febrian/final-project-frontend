import React from 'react';
import Loading from './Loading';

const Table = ({ headers, data, renderRow, isLoading, onRowClick }) => {
  const renderHeaders = () => {
    return headers.map((header) => {
      return (
        <th scope="col" className="py-3 px-6">
          {header}
        </th>
      );
    });
  };

  const renderData = () => {
    return data.map((datum, i) => {
      return (
        <tr
          className="bg-white border-b hover:bg-gray-50"
          onClick={() => {
            onRowClick(datum, i);
          }}
        >
          {renderRow(datum, i)}
        </tr>
      );
    });
  };

  const renderOnLoading = () => {
    return Array(3)
      .fill()
      .map(() => {
        return (
          <tr className="bg-white border-b hover:bg-gray-50">
            {headers.map(() => (
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                <Loading />
              </td>
            ))}
          </tr>
        );
      });
  };

  const render = () => {
    if (isLoading) {
      return renderOnLoading();
    }
    return renderData();
  };

  return (
    <table className="w-full text-sm text-left border-2 border-grey-500">
      <thead className="text-xs text-gray-700 uppercase bg-gray-50">
        <tr>{renderHeaders()}</tr>
      </thead>
      <tbody>{render()}</tbody>
    </table>
  );
};

export default Table;
