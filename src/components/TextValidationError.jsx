import React from 'react';
import PropTypes from 'prop-types';

const TextValidationError = ({ text }) => {
  return <div className="text-red-600 m-0 p-0">{text}</div>;
};

TextValidationError.propTypes = {
  text: PropTypes.string.isRequired,
};

export default TextValidationError;
