import React from 'react';
import PropTypes from 'prop-types';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import TextValidationError from './TextValidationError';
import { FormatDate } from '../utils/TimeFormat';
import { useCreateVoucherMutation } from '../store/slices/apiSlice';

const createVoucherSchema = Yup.object().shape({
  code: Yup.string().min(3, 'Minimum 3 characters').required('Required'),
  percentage: Yup.number()
    .required('Required')
    .min(0, 'Percentage must be between 0 and 100')
    .max(100, 'Percentage must be between 0 and 100'),
  limit: Yup.number().required('Required').min(1000, 'Min. 1000'),
  quota: Yup.number().required('Required').min(1, 'Min. 1 stock'),
  startDate: Yup.date()
    .min(
      new Date(new Date().setHours(0, 0, 0, 0)),
      'Start date must be at least today',
    )
    .required('Required'),
  startTime: Yup.string()
    .required('Required')
    .test(
      'is-greater-than-now',
      'start time should be later than now',
      function (startTime) {
        const { startDate } = this.parent;

        if (!startTime) {
          return false;
        }
        const [hours, mins] = startTime.split(':');
        const startDateTime = new Date(startDate).setHours(+hours, +mins);
        return startDateTime > new Date();
      },
    ),

  expireDate: Yup.date()
    .min(Yup.ref('startDate'), 'Expire date cannot be before start date')
    .required('Required'),

  expireTime: Yup.string()
    .required('Required')
    .test(
      'is-greater-than-active',
      'expire time should be later than active time',
      function (expireTime) {
        const { startDate, startTime, expireDate } = this.parent;

        if (!expireTime) {
          return false;
        }

        if (!startTime) {
          return false;
        }

        const [startHour, startMin] = startTime.split(':');
        const [expireHour, expireMin] = expireTime.split(':');

        const startDateTime = new Date(startDate).setHours(
          +startHour,
          +startMin,
        );
        const expireDateTime = new Date(expireDate).setHours(
          +expireHour,
          +expireMin,
        );

        return expireDateTime > startDateTime;
      },
    ),
});

const CreateVoucherForm = ({ onSuccess, onError, onClose }) => {
  const [createVoucher] = useCreateVoucherMutation();

  const onCreateVoucherSubmit = async (values, { resetForm }) => {
    try {
      const { response, error } = await createVoucher({
        voucherCode: values.code,
        discountPercentage: values.percentage / 100,
        limit: values.limit,
        quota: values.quota,
        startAt: `${values.startDate} ${values.startTime}:00`,
        expireAt: `${values.expireDate} ${values.expireTime}:00`,
      });

      if (error) {
        onError(error.data.endpoint_code);
      }

      if (response) {
        onSuccess();
      }
    } catch (err) {
      console.log(err);
    } finally {
      resetForm();
    }
  };

  const now = new Date();

  return (
    <Formik
      initialValues={{
        code: '',
        percentage: '',
        limit: '',
        quota: '',
        startDate: FormatDate(now.toISOString()),
        expireDate: FormatDate(now.toISOString()),
      }}
      onSubmit={onCreateVoucherSubmit}
      validationSchema={createVoucherSchema}
      validateOnChange
      validateOnBlur
    >
      {({ errors, touched, resetForm }) => (
        <Form className="flex flex-col">
          <div>
            <label htmlFor="code" className="m-1">
              Voucher code
              <Field
                id="code"
                name="code"
                placeholder="Voucher Code (e.g. EZMONEY)"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.code && errors.code && (
                <TextValidationError text={errors.code} />
              )}
            </label>
          </div>

          <div>
            <label htmlFor="percentage" className="m-1">
              Discount Percentage (%)
              <Field
                type="number"
                id="percentage"
                name="percentage"
                placeholder="Voucher percentage (e.g. 50%)"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.percentage && errors.percentage && (
                <TextValidationError text={errors.percentage} />
              )}
            </label>
          </div>

          <div>
            <label htmlFor="limit" className="m-1">
              Voucher Discount Limit
              <Field
                type="number"
                id="limit"
                name="limit"
                placeholder="Voucher limit (e.g. 50000)"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.limit && errors.limit && (
                <TextValidationError text={errors.limit} />
              )}
            </label>
          </div>

          <div>
            <label htmlFor="quota" className="m-1">
              Voucher Quota
              <Field
                type="number"
                id="quota"
                name="quota"
                placeholder="Voucher quota (e.g. 100)"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.quota && errors.quota && (
                <TextValidationError text={errors.quota} />
              )}
            </label>
          </div>

          <div>
            <p>Voucher Start Time</p>
            <div className="flex justify-evenly">
              <div className="w-[50%]">
                <Field
                  type="date"
                  className="py-2 px-4 m-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 "
                  id="startDate"
                  name="startDate"
                />
                {touched.startDate && errors.startDate && (
                  <TextValidationError text={errors.startDate} />
                )}
              </div>
              <div className="w-[50%]">
                <Field
                  type="time"
                  id="startTime"
                  name="startTime"
                  className="py-2 px-4 m-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-[50%]"
                />
                {touched.startTime && errors.startTime && (
                  <TextValidationError text={errors.startTime} />
                )}
              </div>
            </div>
          </div>

          <div>
            <p>Voucher Expire Time</p>
            <div className="flex justify-evenly">
              <div className="w-[50%]">
                <Field
                  type="date"
                  id="expireDate"
                  name="expireDate"
                  className="py-2 px-4 m-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400"
                />
                {touched.expireDate && errors.expireDate && (
                  <TextValidationError text={errors.expireDate} />
                )}
              </div>
              <div className="w-[50%]">
                <Field
                  type="time"
                  id="expireTime"
                  name="expireTime"
                  className="py-2 px-4 m-2 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-[50%]"
                />
                {touched.expireTime && errors.expireTime && (
                  <TextValidationError text={errors.expireTime} />
                )}
              </div>
            </div>
          </div>

          <div className="flex justify-center">
            <button
              type="submit"
              className="mr-2 h-[50px] w-3/12 my-2 bg-secondary rounded-3xl text-white"
            >
              Create
            </button>
            <button
              type="button"
              className="ml-2 h-[50px] w-3/12 my-2 bg-danger rounded-3xl text-white"
              onClick={() => {
                resetForm();
                onClose();
              }}
            >
              Cancel
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

CreateVoucherForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
};

export default CreateVoucherForm;
