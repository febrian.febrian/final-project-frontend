import React from 'react';
import PropTypes from 'prop-types';

const Field = ({ name, children, oneline }) => {
  const flexDirection = oneline ? 'flex-row' : 'flex-col';
  const margin = oneline ? 'mr-1' : 'mb-1';
  return (
    <div className={`flex ${flexDirection}`}>
      <div className={`font-bold ${margin}`}>{name}</div>
      <div>{children}</div>
    </div>
  );
};

Field.propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default Field;
