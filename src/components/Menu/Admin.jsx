import React from 'react';
import { HashLink } from 'react-router-hash-link';
import LogoutButton from '../LogoutButton';
import Menu from './Menu';

const AdminMenu = () => {
  return (
    <Menu>
      <div>
        <HashLink to="/admin#loans" className="text-white">
          Loans
        </HashLink>
      </div>
      <div>
        <HashLink to="/admin#users" className="text-white">
          Users
        </HashLink>
      </div>
      <div>
        <HashLink to="/admin#payments" className="text-white">
          Payments
        </HashLink>
      </div>
      <div>
        <HashLink to="/admin#vouchers" className="text-white">
          Vouchers
        </HashLink>
      </div>
      <div>
        <LogoutButton />
      </div>
    </Menu>
  );
};

export default AdminMenu;
