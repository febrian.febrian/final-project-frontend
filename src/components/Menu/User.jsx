import React from 'react';
import LogoutButton from '../LogoutButton';
import Menu from './Menu';

const UserMenu = () => {
  return (
    <Menu>
      <div>
        <LogoutButton />
      </div>
    </Menu>
  );
};

export default UserMenu;
