import React from 'react';
import PropTypes from 'prop-types';

const Menu = ({ children }) => {
  return (
    <div className="flex flex-row justify-evenly items-center w-4/5">
      {children}
    </div>
  );
};

Menu.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Menu;
