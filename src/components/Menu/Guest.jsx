import React from 'react';
import { useNavigate } from 'react-router-dom';
import FullColorButton from '../FullColorButton';
import Menu from './Menu';

const GuestMenu = () => {
  const navigate = useNavigate();
  const onLoginClick = () => {
    navigate('/login');
  };

  const onRegisterClick = () => {
    navigate('/register');
  };

  return (
    <Menu>
      <div className="flex justify-end">
        <div className="mr-2">
          <FullColorButton
            backgroundColor="bg-secondary"
            onClick={onLoginClick}
          >
            Login
          </FullColorButton>
        </div>
        <div>
          <FullColorButton
            backgroundColor="bg-secondary"
            onClick={onRegisterClick}
          >
            Register
          </FullColorButton>
        </div>
      </div>
    </Menu>
  );
};

export default GuestMenu;
