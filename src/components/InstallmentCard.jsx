import React from 'react';
import { FormatDateTime } from '../utils/TimeFormat';
import Field from './Field';
import FullColorButton from './FullColorButton';
import Loading from './Loading';

const InstallmentCard = ({ installment = {}, isLoading, onPayClick }) => {
  const onPayButtonClick = () => {
    onPayClick(installment);
  };

  const getInstallmentStatus = (installmentObj) => {
    if (installmentObj.payment) {
      return <p className="text-safe font-bold">Paid</p>;
    }

    if (installmentObj.late_by > 0) {
      return (
        <p className="text-danger font-bold">{`Late by ${installmentObj.late_by} days`}</p>
      );
    }
    return <p className="text-green font-bold">Active</p>;
  };

  const renderOnLoading = () => {
    return (
      <div className="block p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100">
        <Field name="Installment for loan">
          <Loading />
        </Field>
        <Field name="Installment Month">
          <Loading />
        </Field>
        <Field name="Status">
          <Loading />
        </Field>
        <Field name="Amount">
          <Loading />
        </Field>
        <Field name="Due Date">
          <Loading />
        </Field>
      </div>
    );
  };

  const renderInstallment = () => {
    return (
      <div className="block p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100 flex flex-col justify-between">
        <div>
          <Field name="Installment for loan">{installment.loan.purpose}</Field>
          <Field name="Installment Month">{installment.month}</Field>
          <Field name="Status">{getInstallmentStatus(installment)}</Field>
          <Field name="Amount">{installment.amount.toLocaleString('id')}</Field>
          {installment.penalty > 0 && (
            <Field name="Penalty">{installment.penalty}</Field>
          )}
          {installment.payment === null && (
            <Field name="Due Date">
              {FormatDateTime(installment.due_date)}
            </Field>
          )}
        </div>
        {installment.payment === null && (
          <div className="mt-3 flex justify-center items-center">
            <FullColorButton
              backgroundColor="bg-secondary"
              onClick={onPayButtonClick}
            >
              Pay
            </FullColorButton>
          </div>
        )}
      </div>
    );
  };

  if (isLoading) {
    return renderOnLoading();
  }

  return renderInstallment();
};

export default InstallmentCard;
