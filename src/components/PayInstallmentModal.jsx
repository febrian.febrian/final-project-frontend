import React, { useEffect, useState } from 'react';
import Field from './Field';
import Modal from './Modal';
import VoucherListModal from './VoucherListModal';

const PayInstallmentModal = ({ installment, onCancel, onPay }) => {
  const [showVoucherModal, setShowVoucherModal] = useState(false);
  const [chosenVoucher, setChosenVoucher] = useState(null);
  const [discountAmount, setDiscountAmount] = useState(0);

  const openVoucherModal = () => {
    setShowVoucherModal(true);
  };

  const closeVoucherModal = () => {
    setShowVoucherModal(false);
  };
  const closeModal = () => {
    closeVoucherModal();
    setChosenVoucher(null);
    setDiscountAmount(0);
    onCancel();
  };

  const onPayInstallment = () => {
    onPay(installment, chosenVoucher);
    closeModal();
  };

  const onUsingVoucher = (voucher) => {
    setChosenVoucher(voucher);
  };

  useEffect(() => {
    if (installment && chosenVoucher) {
      setDiscountAmount(
        Math.min(
          installment.amount * chosenVoucher.discount_rate,
          chosenVoucher.limit,
        ),
      );
    }
  }, [chosenVoucher]);

  return (
    <Modal
      onClose={closeModal}
      show={installment !== null}
      title="Payment"
      body={
        <div>
          <div className="flex flex-col justify-evenly">
            <div>
              <Field name="Amount" oneline>
                {installment?.amount}
              </Field>
              <Field name="Penalty" oneline>
                {installment?.penalty}
              </Field>
              {chosenVoucher && (
                <Field name="Discount from voucher" oneline>
                  {discountAmount}
                </Field>
              )}
              <Field name="Total Amount" oneline>
                {installment
                  ? installment.amount + installment.penalty - discountAmount
                  : 0}
              </Field>
            </div>

            <div className="flex justify-center">
              <VoucherListModal
                show={showVoucherModal === true}
                position="fixed left-2/3"
                width="w-1/4"
                onClose={closeVoucherModal}
                onVoucherChosen={onUsingVoucher}
              />
              <button
                type="button"
                className="py-3 px-0 my-3 border-2 border-gray-400 w-2/3 rounded-3xl"
                onClick={openVoucherModal}
              >
                Find Vouchers
              </button>
            </div>

            <div className="flex justify-center">
              <button
                type="submit"
                className="mr-2 h-[50px] w-3/12 my-2 bg-secondary rounded-3xl text-white"
                onClick={onPayInstallment}
              >
                Pay
              </button>
              <button
                type="button"
                className="ml-2 h-[50px] w-3/12 my-2 bg-danger rounded-3xl text-white"
                onClick={closeModal}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      }
    />
  );
};

export default PayInstallmentModal;
