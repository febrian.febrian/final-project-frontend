import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Editable = ({ onSave, onCancel, children }) => {
  const [showButtons, setShowButtons] = useState(null);

  const onChildrenClick = () => {
    setShowButtons(true);
  };

  const onButtonSave = () => {
    onSave();
    setShowButtons(null);
  };

  const onButtonCancel = () => {
    onCancel();
    setShowButtons(null);
  };

  return (
    <div className="flex flex-row">
      <button type="button" onClick={onChildrenClick}>
        {children}
      </button>
      {showButtons && (
        <div className="flex flex-row">
          <button
            type="button"
            className="px-1 mx-1 text-emerald-500"
            onClick={onButtonSave}
          >
            Save
          </button>
          <button
            type="button"
            className="px-1 mx-1 text-red-500"
            onClick={onButtonCancel}
          >
            Cancel
          </button>
        </div>
      )}
    </div>
  );
};

Editable.defaultProps = {
  onCancel: () => {},
};

Editable.propTypes = {
  onSave: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  children: PropTypes.node.isRequired,
};

export default Editable;
