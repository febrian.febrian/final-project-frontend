import React, { useRef } from 'react';
import PropTypes from 'prop-types';

const Modal = ({ show, title, body, onClose, position, width }) => {
  const modalRef = useRef(null);

  const closeModal = () => {
    onClose();
  };

  const getModalClassName = () => {
    if (show === true) {
      return `block overflow-y-auto overflow-x-hidden ${position} z-50 ${width}`;
    }
    return 'hidden';
  };

  return (
    <div
      ref={modalRef}
      id="defaultModal"
      tabIndex="-1"
      aria-hidden="true"
      className={getModalClassName()}
    >
      <div className="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <div className="relative bg-white rounded-lg shadow">
          <div className="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
            <h3 className="text-xl font-semibold">{title}</h3>
            <button
              type="button"
              className="text-gray-400 bg-transparent rounded-lg text-sm p-1.5 ml-auto inline-flex items-center"
              data-modal-toggle="defaultModal"
              onClick={() => closeModal()}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>

          <div className="p-6 space-y-6">{body}</div>
        </div>
      </div>
    </div>
  );
};

Modal.defaultProps = {
  position: 'fixed top-5 left-1/3',
  width: 'w-1/3',
};

Modal.propTypes = {
  show: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  body: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)])
    .isRequired,
  position: PropTypes.string,
  width: PropTypes.string,
  onClose: PropTypes.func.isRequired,
};

export default Modal;
