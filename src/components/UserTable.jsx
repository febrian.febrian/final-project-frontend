import React, { useEffect, useState } from 'react';
import { FormatDate } from '../utils/TimeFormat';
import Dropdown from './Dropdown';
import PageSelect from './PageSelect';
import Table from './Table';

const UserTable = ({
  users = [],
  isLoading,
  pageCount,
  onFilterChanged,
  onUserClick,
}) => {
  const SORT_BY_JOIN_DATE = 'join_date';
  const SORT_BY_LATE_PAYMENTS = 'late_payments';
  const SORT_BY_CREDIT_USAGE = 'credit_usage';

  const CONTRACT_STATUS_NONE = 'none';
  const CONTRACT_STATUS_ON_PARTNER = 'on_partner';
  const CONTRACT_STATUS_ON_DELIVERY = 'on_delivery';
  const CONTRACT_STATUS_ARRIVED = 'arrived';
  const CONTRACT_STATUS_CONFIRMED = 'confirmed';

  const SORT_ASCENDING = 'asc';
  const SORT_DESCENDING = 'desc';

  const [filter, setFilter] = useState({
    sortBy: SORT_BY_JOIN_DATE,
    sortDirection: SORT_DESCENDING,
    search: '',
    limit: 10,
    page: 1,
    contract: '',
  });

  useEffect(() => {
    onFilterChanged(filter);
  }, [filter]);

  const onSortByChange = (value) => {
    setFilter({
      ...filter,
      sortBy: value,
    });
  };

  const onSortDirectionChange = (value) => {
    setFilter({
      ...filter,
      sortDirection: value,
    });
  };

  const onShowPerPageChange = (value) => {
    setFilter({
      ...filter,
      limit: value,
      page: 1,
    });
  };

  const onSearchChange = (value) => {
    setFilter({
      ...filter,
      search: value,
      page: 1,
    });
  };

  const onContractChange = (contract) => {
    setFilter({
      ...filter,
      contract,
    });
  };

  const onPageChange = (page) => {
    setFilter({
      ...filter,
      page: page,
    });
  };

  return (
    <>
      <div className="flex justify-between items-center pb-4">
        <div className="flex flex-row justify-evenly">
          <Dropdown title="Filter by Contract Status">
            <button type="button" onClick={() => onContractChange('')}>
              All
            </button>
            <button
              type="button"
              onClick={() => onContractChange(CONTRACT_STATUS_NONE)}
            >
              No Contract
            </button>
            <button
              type="button"
              onClick={() => onContractChange(CONTRACT_STATUS_ON_PARTNER)}
            >
              Sent To Partner
            </button>
            <button
              type="button"
              onClick={() => onContractChange(CONTRACT_STATUS_ON_DELIVERY)}
            >
              On Delivery
            </button>
            <button
              type="button"
              onClick={() => onContractChange(CONTRACT_STATUS_ARRIVED)}
            >
              Arrived
            </button>
            <button
              type="button"
              onClick={() => onContractChange(CONTRACT_STATUS_CONFIRMED)}
            >
              Confirmed
            </button>
          </Dropdown>

          <Dropdown title="Sort By">
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_JOIN_DATE)}
            >
              Join Date
            </button>
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_LATE_PAYMENTS)}
            >
              Health
            </button>
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_CREDIT_USAGE)}
            >
              Credit Usage
            </button>
          </Dropdown>

          <Dropdown title="Sort">
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_ASCENDING)}
            >
              Ascending
            </button>
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_DESCENDING)}
            >
              Descending
            </button>
          </Dropdown>
          <Dropdown title="Show per page">
            <button type="button" onClick={() => onShowPerPageChange(5)}>
              5
            </button>
            <button type="button" onClick={() => onShowPerPageChange(10)}>
              10
            </button>
            <button type="button" onClick={() => onShowPerPageChange(20)}>
              20
            </button>
          </Dropdown>

          <div
            id="dropdownRadio"
            className="hidden z-10 w-48 shadow-md bg-white rounded divide-y divide-gray-100 shadow"
            data-popper-reference-hidden=""
            data-popper-escaped=""
            data-popper-placement="bottom"
            style={{
              position: 'absolute',
              inset: '0px auto auto 0px',
              margin: '0px',
              transform: 'translate3d(0px, 1660px, 0px)',
            }}
          >
            <ul
              className="p-3 space-y-1 text-sm text-gray-700"
              aria-labelledby="dropdownRadioButton"
            />
          </div>
        </div>
        <div
          aria-controls="table-search"
          htmlFor="table-search"
          className="sr-only"
        >
          Search
        </div>
        <div className="relative">
          <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg
              className="w-5 h-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <input
            type="text"
            id="table-search"
            className="block p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
            placeholder="Search user name or email"
            onChange={(e) => {
              onSearchChange(e.target.value);
            }}
          />
        </div>
      </div>
      <Table
        headers={[
          'User ID',
          'Name',
          'Email',
          'Health Status',
          'Credit Usage',
          'Credit Limit',
          'Contract Delivery Status',
          'Join Date',
        ]}
        data={users}
        isLoading={isLoading}
        onRowClick={onUserClick}
        renderRow={(user) => {
          return (
            <>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.id}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.name}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.email}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.credit_status.health}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.credit_status.usage.toLocaleString('id')}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.credit_status.limit.toLocaleString('id')}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {user.contract_status_description}
              </td>
              <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
                {FormatDate(user.join_date)}
              </td>
            </>
          );
        }}
      />
      <div className="mt-2 flex justify-center">
        <PageSelect
          count={pageCount}
          currentPage={filter.page}
          onPageChange={onPageChange}
        />
      </div>
    </>
  );
};

export default UserTable;
