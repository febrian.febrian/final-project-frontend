import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Field from './Field';
import FullColorButton from './FullColorButton';
import { FormatDateTime } from '../utils/TimeFormat';
import {
  useApproveLoanMutation,
  useChangeMonthlyDueDateMutation,
  useRejectLoanMutation,
} from '../store/slices/apiSlice';
import Editable from './Editable';
import {
  approveLoanMap,
  changeLoanDueDateMap,
  rejectLoanMap,
} from '../utils/Translator';

const LoanInfo = ({
  id,
  purpose,
  description,
  amount,
  tenor,
  tenorRate,
  totalAmount,
  status,
  monthlyDueDate,

  userName,

  isApproved,

  requestDate,
  approvalDate,

  onError,
  onSuccess,
}) => {
  const [approve] = useApproveLoanMutation();
  const [reject] = useRejectLoanMutation();

  const [selectedDueDate, setSelectedDueDate] = useState(monthlyDueDate);

  const [changeDueDate] = useChangeMonthlyDueDateMutation();

  const showApproveRejectButtonsIfNotApproved = () => {
    const approveLoan = async () => {
      try {
        const { data, error } = await approve(id);

        if (error) {
          onError(approveLoanMap, error.data.endpoint_code);
          return;
        }

        if (data) {
          onSuccess();
        }
      } catch (err) {
        console.log(err);
      }
    };

    const rejectLoan = async () => {
      try {
        const { data, error } = await reject(id);

        if (error) {
          onError(rejectLoanMap, error.data.endpoint_code);
          return;
        }

        if (data) {
          onSuccess();
        }
      } catch (err) {
        console.log(err);
      }
    };

    if (isApproved === false) {
      return (
        <div className="flex justify-center">
          <FullColorButton
            backgroundColor="bg-primary"
            onClick={() => approveLoan()}
          >
            Approve
          </FullColorButton>
          <FullColorButton
            backgroundColor="bg-red-700"
            onClick={() => rejectLoan()}
          >
            Reject
          </FullColorButton>
        </div>
      );
    }
    return <div />;
  };

  const renderSelectDueDateOptions = () => {
    const dates = [];

    for (let i = 1; i <= 30; i += 1) {
      dates.push(i);
    }

    return dates.map((day) => {
      return <option value={day}>{day}</option>;
    });
  };

  const onSelectedDueDateOptionChange = (e) => {
    setSelectedDueDate(e.target.value);
  };

  const onSaveEdit = async () => {
    try {
      const { data, error } = await changeDueDate({
        id,
        date: parseInt(selectedDueDate, 10),
      });
      if (error) {
        onError(changeLoanDueDateMap, error.data.endpoint_code);
        return;
      }
      if (data) {
        onSuccess();
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onCancelEdit = () => {
    setSelectedDueDate(monthlyDueDate);
  };

  return (
    <div className="flex flex-col justify-center border-2 border-black rounded-2xl p-5 m-2 ">
      <div className="flex flex-row justify-between">
        <div className="flex flex-col justify-end mx-1 w-2/4">
          <Field name="Loan ID">{id}</Field>
          <Field name="Loaner">{userName}</Field>
          <Field name="Purpose">{purpose}</Field>
          <Field name="Description">
            <p className="break-words">{description}</p>
          </Field>
          <Field name="Status">{status}</Field>
        </div>
        <div className="flex flex-col justify-end mx-1 w-1/4">
          <Field name="Loan Amount">{amount}</Field>
          <Field name="Tenor">{tenor}</Field>
          <Field name="Rate">{`${tenorRate * 100}%`}</Field>
          <Field name="Total Amount">{totalAmount}</Field>
        </div>
        <div className="flex flex-col justify-end mx-1 w-1/4">
          <Field name="Request Date">{FormatDateTime(requestDate)}</Field>
          <Field name="Approval Date">
            {approvalDate === '-' ? approvalDate : FormatDateTime(approvalDate)}
          </Field>
          <Field name="Monthly Due Date">
            <Editable onSave={onSaveEdit} onCancel={onCancelEdit}>
              <select
                value={selectedDueDate}
                onChange={onSelectedDueDateOptionChange}
              >
                {renderSelectDueDateOptions()}
              </select>
            </Editable>
          </Field>
        </div>
      </div>
      {showApproveRejectButtonsIfNotApproved()}
    </div>
  );
};

LoanInfo.defaultProps = {
  approvalDate: '-',
};

LoanInfo.propTypes = {
  id: PropTypes.number.isRequired,
  purpose: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  tenor: PropTypes.number.isRequired,
  tenorRate: PropTypes.number.isRequired,
  totalAmount: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  monthlyDueDate: PropTypes.number.isRequired,

  userName: PropTypes.string.isRequired,

  isApproved: PropTypes.bool.isRequired,

  requestDate: PropTypes.string.isRequired,
  approvalDate: PropTypes.string,

  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
};

export default LoanInfo;
