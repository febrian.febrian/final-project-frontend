import React, { useEffect, useState } from 'react';
import { useGetActiveVouchersQuery } from '../store/slices/apiSlice';
import translate from '../utils/Translator';
import ErrorBanner from './ErrorBanner';
import Modal from './Modal';
import Voucher from './Voucher';

const VoucherListModal = ({
  show,
  onClose,
  onVoucherChosen,
  position,
  width,
}) => {
  const {
    data: response,
    isFetching,
    error: responseError,
  } = useGetActiveVouchersQuery();
  const [error, setError] = useState(null);

  useEffect(() => {
    if (responseError) {
      setError(translate({}, responseError.data.endpoint_code));
    }
  }, [responseError]);

  const renderVouchersOnLoading = () => {
    return Array(3)
      .fill()
      .map(() => {
        return <Voucher isLoading />;
      });
  };

  const renderVouchers = () => {
    if (error) {
      return <div />;
    }
    return response?.vouchers.map((voucher) => {
      return <Voucher voucher={voucher} onChoose={onVoucherChosen} />;
    });
  };

  return (
    <Modal
      show={show}
      position={position}
      width={width}
      title="Vouchers"
      body={
        <div>
          {error && <ErrorBanner title={error.title} message={error.message} />}
          {isFetching || error ? renderVouchersOnLoading() : renderVouchers()}
        </div>
      }
      onClose={onClose}
    />
  );
};

export default VoucherListModal;
