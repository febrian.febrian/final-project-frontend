import React from 'react';
import PropTypes from 'prop-types';

const ErrorBanner = ({ title, message }) => {
  return (
    <div className="bg-red-200 border-2 border-red-800 p-2">
      <h5 className="text-lg text-red-800 font-bold mb-4">{title}</h5>
      <p className="text-md text-red-800">{message}</p>
    </div>
  );
};

ErrorBanner.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

export default ErrorBanner;
