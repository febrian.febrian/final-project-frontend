import React from 'react';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import TextValidationError from './TextValidationError';
import { useRequestLoanMutation } from '../store/slices/apiSlice';

const requestLoanSchema = Yup.object().shape({
  purpose: Yup.string()
    .required('Required')
    .min(5, 'Minimum 5 characters')
    .max(
      30,
      'Maximum 30 characters. Please input the details in the description',
    ),
  description: Yup.string()
    .required('Required')
    .min(10, 'Please input more details for the loan'),
  amount: Yup.number()
    .required('Required')
    .min(100000, 'Minimum loan request amount is 100.000'),
  tenor: Yup.number()
    .required('Required')
    .oneOf(
      [1, 3, 6, 12],
      'Only tenor for 1, 3, 6, and 12 months are available',
    ),
});

const RequestLoanForm = ({ onSuccess, onError, onClose }) => {
  const [requestLoan] = useRequestLoanMutation();

  const onSendRequestSubmit = async (values) => {
    try {
      const { data: response, error } = await requestLoan({
        purpose: values.purpose,
        description: values.description,
        amount: parseFloat(values.amount),
        tenor: parseInt(values.tenor, 10),
      });

      if (error) {
        onError(error.data.endpoint_code);
      }

      if (response) {
        onSuccess();
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onClosingModal = (resetForm) => {
    resetForm();
    onClose();
  };

  return (
    <Formik
      initialValues={{
        purpose: '',
        description: '',
        amount: '',
      }}
      onSubmit={onSendRequestSubmit}
      validationSchema={requestLoanSchema}
      validateOnChange
      validateOnBlur
    >
      {({ errors, touched, resetForm }) => (
        <Form className="flex flex-col">
          <div>
            <label htmlFor="purpose" className="m-1">
              Purpose
              <Field
                id="purpose"
                name="purpose"
                placeholder="Purpose for the loan"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.purpose && errors.purpose && (
                <TextValidationError text={errors.purpose} />
              )}
            </label>
          </div>

          <div>
            <label htmlFor="description" className="m-1">
              Description
              <Field
                id="description"
                name="description"
                placeholder="Detailed description of the purpose for the loan"
                rows="4"
                as="textarea"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.description && errors.description && (
                <TextValidationError text={errors.description} />
              )}
            </label>
          </div>

          <div>
            <label htmlFor="amount" className="m-1">
              Amount
              <Field
                type="number"
                id="amount"
                name="amount"
                placeholder="Desired amount for the loan"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              />
              {touched.amount && errors.amount && (
                <TextValidationError text={errors.amount} />
              )}
            </label>
          </div>

          <div>
            <label htmlFor="tenor" className="m-1">
              Tenor
              <Field
                id="tenor"
                name="tenor"
                as="select"
                defaultValue="0"
                className="py-2 px-4 bg-inherit rounded-3xl bg-slate-200 border-2 border-slate-400 w-full"
              >
                <option disabled value="0">
                  Select tenor
                </option>
                <option value="1">1 month</option>
                <option value="3">3 months</option>
                <option value="6">6 months</option>
                <option value="12">12 months</option>
              </Field>
              {touched.tenor && errors.tenor && (
                <TextValidationError text={errors.tenor} />
              )}
            </label>
          </div>

          <div className="mt-4 flex justify-center">
            <button
              type="submit"
              className="mr-2 h-[50px] w-3/12 my-2 bg-secondary rounded-3xl text-white"
            >
              Send request
            </button>
            <button
              type="button"
              className="ml-2 h-[50px] w-3/12 my-2 bg-danger rounded-3xl text-white"
              onClick={() => {
                onClosingModal(resetForm);
              }}
            >
              Cancel
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default RequestLoanForm;
