import React from 'react';
import PropTypes from 'prop-types';

const Tabs = ({ index, headers = [], children, onTabChange }) => {
  const getClassName = (i) => {
    if (index === i) {
      return 'inline-block p-4 text-white bg-secondary rounded-t-lg';
    }
    return 'inline-block p-4 text-white bg-primary rounded-t-lg';
  };

  const getChildToShow = () => {
    return children[index];
  };

  const renderTabHeaders = () => {
    return headers.map((header, i) => {
      return (
        <li className="mr-2">
          <button
            type="button"
            aria-current="page"
            className={getClassName(i)}
            onClick={() => {
              onTabChange(i);
            }}
          >
            {header}
          </button>
        </li>
      );
    });
  };

  return (
    <>
      <ul className="flex flex-wrap text-sm font-medium text-center text-white border-b border-primary">
        {renderTabHeaders()}
      </ul>
      <div className="my-3">{getChildToShow()}</div>
    </>
  );
};

Tabs.propTypes = {
  index: PropTypes.number.isRequired,
  headers: PropTypes.arrayOf(PropTypes.string).isRequired,
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  onTabChange: PropTypes.func.isRequired,
};

export default Tabs;
