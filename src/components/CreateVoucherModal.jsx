import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal from './Modal';
import CreateVoucherForm from './CreateVoucherForm';
import ErrorBanner from './ErrorBanner';
import translate, { createVoucherMap } from '../utils/Translator';

const CreateVoucherModal = ({ show, onSuccess, onCancel }) => {
  const [error, setError] = useState(null);

  const onCreateVoucherFail = (endpointCode) => {
    setError(translate(createVoucherMap, endpointCode));

    setTimeout(() => {
      setError(null);
    }, 2000);
  };

  return (
    <Modal
      onClose={onCancel}
      show={show}
      title="Create Voucher"
      body={
        <div>
          {error && <ErrorBanner title={error.title} message={error.message} />}
          <CreateVoucherForm
            onClose={onCancel}
            onError={onCreateVoucherFail}
            onSuccess={onSuccess}
          />
        </div>
      }
    />
  );
};

CreateVoucherModal.propTypes = {
  show: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default CreateVoucherModal;
