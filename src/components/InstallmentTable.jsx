import React from 'react';
import PropTypes from 'prop-types';
import { FormatDateTime } from '../utils/TimeFormat';

const InstallmentTable = ({ installments = [] }) => {
  const getStatusElement = (installment) => {
    console.log(installment.payment);
    if (installment.payment) {
      return <p className="text-safe font-bold">Paid</p>;
    }

    if (installment.late_by > 0) {
      return (
        <p className="text-danger font-bold">{`Late by ${installment.late_by} days`}</p>
      );
    }

    if (new Date(installment.active_date) < Date.now()) {
      return <p className="text-green font-bold">Active</p>;
    }

    return <p className="text-inactive font-bold">Inactive</p>;
  };

  const renderData = () => {
    return installments.map((installment) => {
      return (
        <tr className="bg-white border-b hover:bg-gray-50">
          <td className="py-4 px-6">{installment.id}</td>
          <td className="py-4 px-6">{installment.amount}</td>
          <td className="py-4 px-6">{installment.penalty}</td>
          <td className="py-4 px-6">{getStatusElement(installment)}</td>
          <td className="py-4 px-6">{installment.late_by}</td>
          <td className="py-4 px-6">
            {FormatDateTime(installment.active_date)}
          </td>
          <td className="py-4 px-6">{FormatDateTime(installment.due_date)}</td>
          <td className="py-4 px-6">
            {installment.payment === null
              ? '-'
              : FormatDateTime(installment.payment.payment_at)}
          </td>
        </tr>
      );
    });
  };

  return (
    <table className="w-full text-sm text-left border-2 border-grey-500">
      <thead className="text-xs text-gray-700 uppercase bg-gray-50">
        <tr>
          <th scope="col" className="py-3 px-6">
            Installment ID
          </th>
          <th scope="col" className="py-3 px-6">
            Amount
          </th>
          <th scope="col" className="py-3 px-6">
            Penalty
          </th>
          <th scope="col" className="py-3 px-6">
            Status
          </th>
          <th scope="col" className="py-3 px-6">
            Late (in days)
          </th>
          <th scope="col" className="py-3 px-6">
            Active Time
          </th>
          <th scope="col" className="py-3 px-6">
            Due Time
          </th>
          <th scope="col" className="py-3 px-6">
            Paid At
          </th>
        </tr>
      </thead>
      <tbody>{renderData()}</tbody>
    </table>
  );
};

InstallmentTable.propTypes = {
  installments: PropTypes.array.isRequired,
};

export default InstallmentTable;
