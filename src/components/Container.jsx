import React from 'react';
import PropTypes from 'prop-types';

const Container = ({ width, children }) => {
  return (
    <div className={`${width} flex flex-col justify-center items-center`}>
      {children}
    </div>
  );
};

Container.propTypes = {
  width: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Container;
