import React from 'react';
import { useConfirmContractMutation } from '../store/slices/apiSlice';
import Banner from './Banner';
import Loading from './Loading';

const ContractBanner = ({ status, isLoading, onError }) => {
  const [confirmContract] = useConfirmContractMutation();

  const onConfirmContract = async () => {
    try {
      const { error } = await confirmContract();
      if (error) {
        onError(error.data.endpointCode);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const statusMap = {
    1: {
      title: 'No contract has been issued yet',
      message: 'We will send you a contract soon! Kindly wait',
    },
    2: {
      title: 'Contract is sent to expedition',
      message: 'The contract is sent to our expedition partner',
    },
    3: {
      title: 'Contract is on delivery',
      message: 'The contract is currently on the way to you!',
    },
    4: {
      title: 'Contract is received by user',
      message:
        'The contract is delivered to you. You can confirm if you agree with the contract',
    },
    5: {
      title: 'Contract is confirmed',
      message: 'You agreed to the contract. Now you can request for a loan',
    },
  };

  const bannerText = statusMap[status];

  return (
    <Banner
      backgroundColor="bg-orange-200"
      borderColor="border-amber-700"
      textColor="text-amber-700"
      title={isLoading ? <Loading /> : bannerText?.title}
    >
      {status === 4 ? (
        <div className="flex items-end w-full">
          <p className="text-md text-amber-700 break-all max-w-[80%]">
            {isLoading ? <Loading /> : bannerText?.message}
          </p>
          <button
            type="button"
            onClick={onConfirmContract}
            className="bg-primary h-10 px-2 height-[20px] aspect-[21/9] text-white font-bold rounded-2xl"
          >
            Confirm
          </button>
        </div>
      ) : (
        <div className="flex items-end w-full">
          <p className="text-md text-amber-700 break-all">
            {isLoading ? <Loading /> : bannerText?.message}
          </p>
        </div>
      )}
    </Banner>
  );
};

export default ContractBanner;
