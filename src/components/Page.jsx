import React from 'react';
import PropTypes from 'prop-types';
import Container from './Container';

const Page = ({ width, children }) => {
  return (
    <Container width="w-4/5">
      <div className={`py-5 ${width}`}>{children}</div>
    </Container>
  );
};

Page.defaultProps = {
  width: 'w-full',
};

Page.propTypes = {
  children: PropTypes.node.isRequired,
  width: PropTypes.string,
};

export default Page;
