import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import useOutOfComponentClickListener from '../hooks/OutOfComponentClickListener';
import Loading from './Loading';

const Dropdown = ({ children, title }) => {
  const [show, setShow] = useState(false);

  const outsideClickRef = useRef(null);

  useOutOfComponentClickListener(outsideClickRef, () => {
    setShow(false);
  });

  const getDisplayStyle = () => {
    return show ? 'block' : 'hidden';
  };

  const onDropdownClick = () => {
    setShow(!show);
  };

  const renderChildren = () => {
    if (children) {
      return children.map((child) => {
        return <div className="block py-2 px-4 hover:bg-teal-500">{child}</div>;
      });
    }
    return <Loading />;
  };

  return (
    <div ref={outsideClickRef} className="relative">
      <button
        className="text-white bg-primary hover:bg-teal-800 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center"
        onClick={onDropdownClick}
        type="button"
      >
        {title}
        <svg
          className="ml-2 w-4 h-4"
          aria-hidden="true"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M19 9l-7 7-7-7"
          />
        </svg>
      </button>
      <div
        id="dropdown"
        className={`${getDisplayStyle()} absolute z-10 w-44 bg-white rounded divide-y divide-gray-100 shadow dark:bg-teal-700`}
      >
        <div
          className="py-1 text-sm text-gray-700 dark:text-gray-200"
          aria-labelledby="dropdownDefault"
        >
          {renderChildren()}
        </div>
      </div>
    </div>
  );
};

Dropdown.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  title: PropTypes.string.isRequired,
};

export default Dropdown;
