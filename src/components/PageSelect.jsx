import React from 'react';
import PropTypes from 'prop-types';

const PageSelect = ({ count, onPageChange, currentPage }) => {
  const onPageSelectChange = (page) => {
    onPageChange(page);
  };

  const renderPageSelections = () => {
    const buttons = [];

    if (count > 0) {
      buttons.push(
        <PageSelectButton
          key="begin"
          text="<<"
          onClick={() => onPageSelectChange(1)}
        />,
      );
    }

    for (let i = 1; i <= count; i += 1) {
      buttons.push(
        <PageSelectButton
          key={i}
          selected={currentPage === i}
          text={i}
          onClick={() => onPageSelectChange(i)}
        />,
      );
    }

    if (count > 0) {
      buttons.push(
        <PageSelectButton
          key="end"
          text=">>"
          onClick={() => onPageSelectChange(count)}
        />,
      );
    }

    return buttons;
  };

  return <div className="flex flex-row">{renderPageSelections()}</div>;
};

PageSelect.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired,
};

export default PageSelect;

const PageSelectButton = ({ text, onClick, selected }) => {
  const getSelectedStyle = () => {
    if (selected !== true) {
      return 'border-2 border-solid-black text-black';
    }
    return 'border-1 border-teal-600 bg-teal-600 text-white';
  };

  return (
    <button
      type="button"
      className={`${getSelectedStyle()} h-[35px] w-[35px]`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

PageSelectButton.defaultProps = {
  selected: false,
};

PageSelectButton.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool,
};
