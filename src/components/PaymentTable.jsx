import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FormatDateTime } from '../utils/TimeFormat';
import Dropdown from './Dropdown';
import Loading from './Loading';
import PageSelect from './PageSelect';

const PaymentTable = ({
  payments = [],
  isLoading,
  pageCount,
  onFilterChanged,
}) => {
  const SORT_BY_PAYMENT_TIME = 'payment_time';
  const SORT_BY_AMOUNT = 'amount';

  const SORT_ASCENDING = 'asc';
  const SORT_DESCENDING = 'desc';

  const SHOW_24H = '24h';
  const SHOW_7D = '7d';
  const SHOW_30D = '30d';
  const SHOW_ALL_TIME = 'all_time';

  const [filter, setFilter] = useState({
    sortBy: SORT_BY_PAYMENT_TIME,
    sortDirection: SORT_DESCENDING,
    search: '',
    limit: 10,
    page: 1,
    show: SHOW_24H,
  });

  useEffect(() => {
    onFilterChanged(filter);
  }, [filter]);

  const onSortByChange = (value) => {
    setFilter({
      ...filter,
      sortBy: value,
    });
  };

  const onSortDirectionChange = (value) => {
    setFilter({
      ...filter,
      sortDirection: value,
    });
  };

  const onShowPerPageChange = (value) => {
    setFilter({
      ...filter,
      limit: value,
    });
  };

  const onSearchChange = (value) => {
    setFilter({
      ...filter,
      search: value,
      page: 1,
    });
  };

  const onShowChange = (show) => {
    setFilter({
      ...filter,
      show: show,
    });
  };

  const onPageChange = (page) => {
    setFilter({
      ...filter,
      page: page,
    });
  };

  const renderData = () => {
    if (isLoading) {
      return Array(3)
        .fill()
        .map(() => {
          return (
            <tr className="bg-white border-b hover:bg-gray-50">
              <td className="py-4 px-6">
                <Loading />
              </td>
              <td className="py-4 px-6">
                <Loading />
              </td>
              <td className="py-4 px-6">
                <Loading />
              </td>
              <td className="py-4 px-6">
                <Loading />
              </td>
              <td className="py-4 px-6">
                <Loading />
              </td>
              <td className="py-4 px-6">
                <Loading />
              </td>
              <td className="py-4 px-6">
                <Loading />
              </td>
            </tr>
          );
        });
    }
    return payments.map((payment) => {
      return (
        <tr key={payment.id} className="bg-white border-b hover:bg-gray-50">
          <td className="py-4 px-6">{payment.id}</td>
          <td className="py-4 px-6">{payment.installment.loan_id}</td>
          <td className="py-4 px-6">{payment.installment.id}</td>
          <td className="py-4 px-6">{payment.user.name}</td>
          <td className="py-4 px-6">{payment.amount}</td>
          <td className="py-4 px-6">{FormatDateTime(payment.payment_at)}</td>
          <td className="py-4 px-6">{payment.voucher?.voucher_code ?? '-'}</td>
        </tr>
      );
    });
  };

  return (
    <div>
      <div className="flex justify-between items-center pb-4">
        <div className="flex flex-row justify-evenly">
          <Dropdown title="Show">
            <button type="button" onClick={() => onShowChange(SHOW_24H)}>
              Last 24 hours
            </button>
            <button type="button" onClick={() => onShowChange(SHOW_7D)}>
              Last 7 days
            </button>
            <button type="button" onClick={() => onShowChange(SHOW_30D)}>
              Last 30 days
            </button>
            <button type="button" onClick={() => onShowChange(SHOW_ALL_TIME)}>
              All time
            </button>
          </Dropdown>

          <Dropdown title="Sort By">
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_PAYMENT_TIME)}
            >
              Payment Time
            </button>
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_AMOUNT)}
            >
              Amount
            </button>
          </Dropdown>

          <Dropdown title="Sort">
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_ASCENDING)}
            >
              Ascending
            </button>
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_DESCENDING)}
            >
              Descending
            </button>
          </Dropdown>
          <Dropdown title="Show per page">
            <button type="button" onClick={() => onShowPerPageChange(5)}>
              5
            </button>
            <button type="button" onClick={() => onShowPerPageChange(10)}>
              10
            </button>
            <button type="button" onClick={() => onShowPerPageChange(20)}>
              20
            </button>
          </Dropdown>

          <div
            id="dropdownRadio"
            className="hidden z-10 w-48 shadow-md bg-white rounded divide-y divide-gray-100 shadow"
            data-popper-reference-hidden=""
            data-popper-escaped=""
            data-popper-placement="bottom"
            style={{
              position: 'absolute',
              inset: '0px auto auto 0px',
              margin: '0px',
              transform: 'translate3d(0px, 1660px, 0px)',
            }}
          >
            <ul
              className="p-3 space-y-1 text-sm text-gray-700"
              aria-labelledby="dropdownRadioButton"
            />
          </div>
        </div>
        <div
          aria-controls="table-search"
          htmlFor="table-search"
          className="sr-only"
        >
          Search
        </div>
        <div className="relative">
          <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg
              className="w-5 h-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <input
            type="text"
            id="table-search"
            className="block p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
            placeholder="Search user name"
            onChange={(e) => {
              onSearchChange(e.target.value);
            }}
          />
        </div>
      </div>

      <table className="w-full text-sm text-left border-2 border-grey-500">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50">
          <tr>
            <th scope="col" className="py-3 px-6">
              Payment ID
            </th>
            <th scope="col" className="py-3 px-6">
              Loan ID
            </th>
            <th scope="col" className="py-3 px-6">
              Installment Month
            </th>
            <th scope="col" className="py-3 px-6">
              Name
            </th>
            <th scope="col" className="py-3 px-6">
              Amount
            </th>
            <th scope="col" className="py-3 px-6">
              Payment At
            </th>
            <th scope="col" className="py-3 px-6">
              Voucher Code Used
            </th>
          </tr>
        </thead>
        <tbody>{renderData()}</tbody>
      </table>

      <div className="mt-2 flex justify-center">
        <PageSelect
          count={pageCount}
          currentPage={filter.page}
          onPageChange={onPageChange}
        />
      </div>
    </div>
  );
};

PaymentTable.propTypes = {
  payments: PropTypes.arrayOf(PropTypes.object).isRequired,
  isLoading: PropTypes.bool.isRequired,
  pageCount: PropTypes.number.isRequired,
  onFilterChanged: PropTypes.func.isRequired,
};

export default PaymentTable;
