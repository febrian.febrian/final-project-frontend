import React from 'react';

const LoanStatusText = ({ status }) => {
  if (status === 'pending') {
    return <p className="text-pending font-bold">Pending</p>;
  }
  if (status === 'rejected') {
    return <p className="text-danger font-bold">Rejected</p>;
  }
  if (status === 'active') {
    return <p className="text-green font-bold">Active</p>;
  }

  return <p className="text-safe font-bold">Done</p>;
};

export default LoanStatusText;
