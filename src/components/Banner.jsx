import React from 'react';

const Banner = ({
  borderColor,
  backgroundColor,
  textColor,
  title,
  children,
}) => {
  return (
    <div
      className={`min-w-[30%] max-w-[40%] ${backgroundColor} border-2 ${borderColor} p-2`}
    >
      <h5 className={`text-lg ${textColor} font-bold mb-4`}>{title}</h5>
      {children}
    </div>
  );
};

export default Banner;
