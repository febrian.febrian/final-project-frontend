import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import Dropdown from '../Dropdown';
import Loading from '../Loading';
import PageSelect from '../PageSelect';
import { FormatDateTime } from '../../utils/TimeFormat';

const LoanTable = ({ response, onFilterChange }) => {
  const SORT_BY_AMOUNT = 'amount';
  const SORT_BY_REQUEST_DATE = 'request_date';

  const SORT_ASCENDING = 'asc';
  const SORT_DESCENDING = 'desc';

  const [filter, setFilter] = useState({
    sortBy: SORT_BY_REQUEST_DATE,
    sortDirection: SORT_DESCENDING,
    search: '',
    limit: 10,
    page: 1,
  });

  const navigate = useNavigate();

  useEffect(() => {
    onFilterChange(filter);
  }, [onFilterChange]);

  useEffect(() => {
    onFilterChange(filter);
  }, [filter]);

  const onSortByChange = (value) => {
    setFilter({
      ...filter,
      sortBy: value,
    });
  };

  const onSortDirectionChange = (value) => {
    setFilter({
      ...filter,
      sortDirection: value,
    });
  };

  const onShowPerPageChange = (value) => {
    setFilter({
      ...filter,
      limit: value,
    });
  };

  const onSearchChange = (e) => {
    setFilter({
      ...filter,
      search: e.target.value,
      page: 1,
    });
  };

  const onLoansPageChange = (page) => {
    setFilter({
      ...filter,
      page: page,
    });
  };

  const onTableRowClick = (id) => {
    navigate(`/admin/loans/${id}`);
  };

  const renderDataTable = () => {
    if (response) {
      return response.loans.map((entry) => {
        return (
          <tr
            className="bg-white border-b hover:bg-gray-50 cursor-pointer"
            onClick={() => {
              onTableRowClick(entry.id);
            }}
          >
            <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
              {entry.id}
            </td>
            <td className="py-4 px-6">{entry.user.name}</td>
            <td className="py-4 px-6">{entry.purpose}</td>
            <td className="py-4 px-6">{entry.amount}</td>
            <td className="py-4 px-6">{entry.tenor}</td>
            <td className="py-4 px-6">{FormatDateTime(entry.request_date)}</td>
          </tr>
        );
      });
    }
    return (
      <tr className="bg-white border-b hover:bg-gray-50">
        <td className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap">
          <Loading />
        </td>
        <td className="py-4 px-6">
          <Loading />
        </td>
        <td className="py-4 px-6">
          <Loading />
        </td>
        <td className="py-4 px-6">
          <Loading />
        </td>
        <td className="py-4 px-6">
          <Loading />
        </td>
        <td className="py-4 px-6">
          <Loading />
        </td>
      </tr>
    );
  };
  return (
    <div className="overflow-x-auto relative sm:rounded-lg">
      <div className="flex justify-between items-center pb-4">
        <div className="flex flex-row justify-evenly">
          <Dropdown title="Sort By">
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_AMOUNT)}
            >
              Amount
            </button>
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_REQUEST_DATE)}
            >
              Request Date
            </button>
          </Dropdown>

          <Dropdown title="Sort">
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_ASCENDING)}
            >
              Ascending
            </button>
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_DESCENDING)}
            >
              Descending
            </button>
          </Dropdown>
          <Dropdown title="Show per page">
            <button type="button" onClick={() => onShowPerPageChange(5)}>
              5
            </button>
            <button type="button" onClick={() => onShowPerPageChange(10)}>
              10
            </button>
            <button type="button" onClick={() => onShowPerPageChange(20)}>
              20
            </button>
          </Dropdown>

          <div
            id="dropdownRadio"
            className="hidden z-10 w-48 shadow-md bg-white rounded divide-y divide-gray-100 shadow"
            data-popper-reference-hidden=""
            data-popper-escaped=""
            data-popper-placement="bottom"
            style={{
              position: 'absolute',
              inset: '0px auto auto 0px',
              margin: '0px',
              transform: 'translate3d(0px, 1660px, 0px)',
            }}
          >
            <ul
              className="p-3 space-y-1 text-sm text-gray-700"
              aria-labelledby="dropdownRadioButton"
            />
          </div>
        </div>
        <div
          aria-controls="table-search"
          htmlFor="table-search"
          className="sr-only"
        >
          Search
        </div>
        <div className="relative">
          <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg
              className="w-5 h-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <input
            type="text"
            id="table-search"
            className="block p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
            placeholder="Search for items"
            onChange={onSearchChange}
          />
        </div>
      </div>

      <table className="w-full text-sm text-left border-2 border-grey-500">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50">
          <tr>
            <th scope="col" className="py-3 px-6">
              Loan ID
            </th>
            <th scope="col" className="py-3 px-6">
              Loaner
            </th>
            <th scope="col" className="py-3 px-6">
              Purpose
            </th>
            <th scope="col" className="py-3 px-6">
              Amount
            </th>
            <th scope="col" className="py-3 px-6">
              Tenor
            </th>
            <th scope="col" className="py-3 px-6">
              Request Date
            </th>
          </tr>
        </thead>
        <tbody>{renderDataTable()}</tbody>
      </table>
      <div className="flex flex-row justify-center py-2">
        {response && (
          <PageSelect
            currentPage={filter.page}
            count={response.total_page}
            onPageChange={onLoansPageChange}
          />
        )}
      </div>
    </div>
  );
};

LoanTable.defaultProps = {
  response: null,
};

LoanTable.propTypes = {
  response: PropTypes.object,
  onFilterChange: PropTypes.func.isRequired,
};

export default LoanTable;
