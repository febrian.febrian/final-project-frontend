import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Toggle = ({
  id,
  enabled,
  onDisablingAsync,
  onEnablingAsync,
  cancelOnFail,
}) => {
  const [isEnabled, setIsEnabled] = useState(enabled);

  const getFn = {
    true: onDisablingAsync,
    false: onEnablingAsync,
  };

  const toggleWithCancel = async () => {
    const isSuccess = await getFn[isEnabled]();
    if (isSuccess) {
      setIsEnabled(!isEnabled);
    }
  };

  const toggleNaive = () => {
    getFn[isEnabled]();
    setIsEnabled(!isEnabled);
  };

  const toggle = () => {
    if (cancelOnFail) {
      toggleWithCancel();
      return;
    }
    toggleNaive();
  };

  return (
    <label
      htmlFor={`toggle-${id}`}
      className="inline-flex relative items-center mr-5 cursor-pointer"
    >
      <input
        type="checkbox"
        id={`toggle-${id}`}
        className="sr-only peer"
        onChange={() => {
          toggle();
        }}
        checked={isEnabled}
      />
      <div className="w-11 h-6 bg-gray-400 rounded-full peer peer-focus:ring-4 peer-focus:ring-green-300 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all peer-checked:bg-secondary" />
    </label>
  );
};

Toggle.defaultProps = {
  cancelOnFail: true,
};

Toggle.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  enabled: PropTypes.bool.isRequired,
  onEnablingAsync: PropTypes.func.isRequired,
  onDisablingAsync: PropTypes.func.isRequired,
  cancelOnFail: PropTypes.bool,
};

export default Toggle;
