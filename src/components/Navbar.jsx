import React, { useState, useEffect } from 'react';
import { useGetAuthenticatedUserQuery } from '../store/slices/apiSlice';
import Logo from './Logo';
import AdminMenu from './Menu/Admin';
import GuestMenu from './Menu/Guest';
import UserMenu from './Menu/User';

const Navbar = () => {
  const { data: response, error } = useGetAuthenticatedUserQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  const [user, setUser] = useState(null);
  const [redirect, setRedirect] = useState('/login');

  useEffect(() => {
    if (response) {
      setUser(response);
    }
  }, [response]);

  useEffect(() => {
    if (error) {
      setUser(null);
    }
  }, [error]);

  useEffect(() => {
    if (user === null) {
      setRedirect('/login');
      return;
    }
    if (user.role === 'admin') {
      setRedirect('/admin');
    } else {
      setRedirect('/');
    }
  }, [user]);

  const renderMenu = () => {
    if (user === null) {
      return <GuestMenu />;
    }
    if (user.role === 'admin') {
      return <AdminMenu />;
    }
    return <UserMenu />;
  };

  return (
    <div className="h-[75px] w-full bg-primary flex items-center justify-center">
      <div className="w-4/5 flex flex-row items-center justify-between">
        <div className="grow-[2]">
          <Logo redirectTo={redirect} />
        </div>
        <div className="grow">{renderMenu()}</div>
      </div>
    </div>
  );
};

export default Navbar;
