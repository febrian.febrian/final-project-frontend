import React from 'react';
import Field from './Field';
import { FormatDateTime } from '../utils/TimeFormat';
import Loading from './Loading';
import LoanStatusText from './LoanStatusText';

const LoanCard = ({ loan, isLoading }) => {
  const renderOnLoading = () => {
    return (
      <div className="block p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100">
        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
          <Loading />
        </h5>
        <div>
          <Field name="Status">
            <Loading />
          </Field>
          <Field name="Loan amount">
            <Loading />
          </Field>
          <Field name="Tenor">
            <Loading />
          </Field>
          <Field name="Tenor Rate">
            <Loading />
          </Field>
          <Field name="Total Amount">
            <Loading />
          </Field>
          <Field name="Request Date">
            <Loading />
          </Field>
          <Field name="Approval Date">
            <Loading />
          </Field>
        </div>
      </div>
    );
  };

  if (isLoading) {
    return renderOnLoading();
  }

  return (
    <div className="block p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md hover:bg-gray-100">
      <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
        {loan.purpose}
      </h5>
      <div>
        <Field name="Status">
          <LoanStatusText status={loan.status} />
        </Field>
        <Field name="Loan amount">{loan.amount.toLocaleString('id')}</Field>
        <Field name="Tenor">{`${loan.tenor} months`}</Field>
        <Field name="Tenor Rate">{`${loan.tenor_rate * 100}%`}</Field>
        <Field name="Total Amount">
          {loan.total_amount.toLocaleString('id')}
        </Field>
        <Field name="Request Date">{FormatDateTime(loan.request_date)}</Field>
        <Field name="Approval Date">
          {loan.approval_date ? FormatDateTime(loan.approval_date) : '-'}
        </Field>
      </div>
    </div>
  );
};

export default LoanCard;
