import React from 'react';
import { useNavigate } from 'react-router-dom';
import FullColorButton from './FullColorButton';

const LogoutButton = () => {
  const navigate = useNavigate();

  const deleteToken = async () => {
    localStorage.removeItem('token');

    navigate('/login', { replace: true });
  };

  return (
    <FullColorButton backgroundColor="bg-secondary" onClick={deleteToken}>
      Logout
    </FullColorButton>
  );
};

export default LogoutButton;
