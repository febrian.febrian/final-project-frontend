import React from 'react';
import PropTypes from 'prop-types';
import Guard from './Guard';

const AuthGuard = ({ children }) => {
  return (
    <Guard allow={(isAuth) => isAuth} navigateOnFail="/login">
      {children}
    </Guard>
  );
};

AuthGuard.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AuthGuard;
