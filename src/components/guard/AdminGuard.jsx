import React from 'react';
import PropTypes from 'prop-types';
import Guard from './Guard';

const AdminGuard = ({ children }) => {
  const allow = (isAuth, user) => {
    if (!isAuth) {
      return [false, '/login'];
    }
    if (user.role !== 'admin') {
      return [false, '/'];
    }
    return [true, null];
  };

  return <Guard allow={allow}>{children}</Guard>;
};

AdminGuard.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AdminGuard;
