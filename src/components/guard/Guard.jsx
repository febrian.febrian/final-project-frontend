import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { useGetAuthenticatedUserQuery } from '../../store/slices/apiSlice';

const Guard = ({ allow, children }) => {
  const { data: user, error } = useGetAuthenticatedUserQuery(null, {
    refetchOnMountOrArgChange: true,
  });

  const navigate = useNavigate();

  useEffect(() => {
    if (user) {
      const [isAllowed, redirectTo] = allow(true, user);
      if (!isAllowed) {
        navigate(redirectTo, { replace: true });
      }
    }
  }, [user]);

  useEffect(() => {
    if (error) {
      if (error.data.endpoint_code !== 11) {
        console.log('ERROR. SERVER', error);
      }

      const [isAllowed, redirectTo] = allow(false, null);
      if (!isAllowed) {
        navigate(redirectTo, { replace: true });
      }
    }
  }, [error]);

  return children;
};

Guard.propTypes = {
  allow: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default Guard;
