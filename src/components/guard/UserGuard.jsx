import React from 'react';
import PropTypes from 'prop-types';
import Guard from './Guard';

const UserGuard = ({ children }) => {
  const allow = (isAuth, user) => {
    if (!isAuth) {
      return [false, '/login'];
    }
    if (user.role !== 'user') {
      return [false, '/admin'];
    }

    return [true, null];
  };

  return <Guard allow={allow}>{children}</Guard>;
};

UserGuard.propTypes = {
  children: PropTypes.node.isRequired,
};

export default UserGuard;
