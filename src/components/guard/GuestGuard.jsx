import React from 'react';
import PropTypes from 'prop-types';
import Guard from './Guard';

const GuestGuard = ({ children }) => {
  const allow = (isAuth, user) => {
    const storage = localStorage.getItem('token');
    if (!storage || !isAuth) {
      return [true, null];
    }

    if (user.role === 'admin') {
      return [false, '/admin'];
    }
    return [false, '/'];
  };

  return <Guard allow={allow}>{children}</Guard>;
};

GuestGuard.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GuestGuard;
