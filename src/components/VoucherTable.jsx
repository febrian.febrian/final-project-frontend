import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FormatDateTime } from '../utils/TimeFormat';
import Toggle from './Toggle';
import { useUpdateVoucherActiveStatusMutation } from '../store/slices/apiSlice';
import Dropdown from './Dropdown';
import PageSelect from './PageSelect';
import Loading from './Loading';

const VOUCHER_ACTIVATE_STATUS = 'active';
const VOUCHER_DISABLE_STATUS = 'disabled';

const SORT_DIRECTION_DESCENDING = 'desc';
const SORT_DIRECTION_ASCENDING = 'asc';

const SORT_BY_ACTIVE_STATUS = 'active';
const SORT_BY_START_TIME = 'active_time';
const SORT_BY_EXPIRE_TIME = 'expiry_time';

const VoucherTable = ({ vouchers, isLoading, pageCount, onFilterChanged }) => {
  const [updateVoucherStatus] = useUpdateVoucherActiveStatusMutation();
  const [filter, setFilter] = useState({
    search: '',
    limit: 10,
    page: 1,
    sortBy: SORT_BY_START_TIME,
    sortDirection: SORT_DIRECTION_DESCENDING,
  });

  useEffect(() => {
    onFilterChanged(filter);
  }, [filter]);

  const onSearchChange = (value) => {
    setFilter({
      ...filter,
      search: value,
    });
  };

  const onSortDirectionChange = (direction) => {
    setFilter({
      ...filter,
      sortDirection: direction,
    });
  };

  const onSortByChange = (sortBy) => {
    setFilter({
      ...filter,
      sortBy: sortBy,
    });
  };

  const onShowPerPageChange = (limit) => {
    setFilter({
      ...filter,
      limit: limit,
    });
  };

  const onPageChange = (page) => {
    setFilter({
      ...filter,
      page: page,
    });
  };

  const disableVoucher = async (id) => {
    try {
      const { error } = await updateVoucherStatus({
        id,
        status: VOUCHER_DISABLE_STATUS,
      });
      if (error) {
        console.log(error);
        return false;
      }
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  };

  const enableVoucher = async (id) => {
    try {
      const { error } = await updateVoucherStatus({
        id,
        status: VOUCHER_ACTIVATE_STATUS,
      });
      if (error) {
        console.log(error);
        return false;
      }
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  };

  const renderData = () => {
    if (isLoading) {
      return Array(3)
        .fill()
        .map(() => (
          <tr className="bg-white border-b hover:bg-gray-50">
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
            <td className="py-4 px-6">
              <Loading />
            </td>
          </tr>
        ));
    }
    return vouchers.map((voucher) => {
      return (
        <tr key={voucher.id} className="bg-white border-b hover:bg-gray-50">
          <td className="py-4 px-6">{voucher.id}</td>
          <td className="py-4 px-6">{voucher.voucher_code}</td>
          <td className="py-4 px-6">{`${voucher.discount_rate * 100}%`}</td>
          <td className="py-4 px-6">{voucher.limit}</td>
          <td className="py-4 px-6">{voucher.quota}</td>
          <td className="py-4 px-6">
            <Toggle
              id={voucher.id}
              enabled={voucher.is_active}
              onEnablingAsync={async () => {
                return enableVoucher(voucher.id);
              }}
              onDisablingAsync={async () => {
                return disableVoucher(voucher.id);
              }}
              cancelOnFail
            />
          </td>
          <td className="py-4 px-6">{FormatDateTime(voucher.active_at)}</td>
          <td className="py-4 px-6">{FormatDateTime(voucher.expires_at)}</td>
        </tr>
      );
    });
  };
  return (
    <div>
      <div className="flex justify-between items-center pb-4">
        <div className="flex flex-row justify-evenly">
          <Dropdown title="Sort By">
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_ACTIVE_STATUS)}
            >
              Active
            </button>
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_START_TIME)}
            >
              Start Time
            </button>
            <button
              type="button"
              onClick={() => onSortByChange(SORT_BY_EXPIRE_TIME)}
            >
              Expire Time
            </button>
          </Dropdown>

          <Dropdown title="Sort">
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_DIRECTION_ASCENDING)}
            >
              Ascending
            </button>
            <button
              type="button"
              onClick={() => onSortDirectionChange(SORT_DIRECTION_DESCENDING)}
            >
              Descending
            </button>
          </Dropdown>
          <Dropdown title="Show per page">
            <button type="button" onClick={() => onShowPerPageChange(5)}>
              5
            </button>
            <button type="button" onClick={() => onShowPerPageChange(10)}>
              10
            </button>
            <button type="button" onClick={() => onShowPerPageChange(20)}>
              20
            </button>
          </Dropdown>

          <div
            id="dropdownRadio"
            className="hidden z-10 w-48 shadow-md bg-white rounded divide-y divide-gray-100 shadow"
            data-popper-reference-hidden=""
            data-popper-escaped=""
            data-popper-placement="bottom"
            style={{
              position: 'absolute',
              inset: '0px auto auto 0px',
              margin: '0px',
              transform: 'translate3d(0px, 1660px, 0px)',
            }}
          >
            <ul
              className="p-3 space-y-1 text-sm text-gray-700"
              aria-labelledby="dropdownRadioButton"
            />
          </div>
        </div>
        <div
          aria-controls="table-search"
          htmlFor="table-search"
          className="sr-only"
        >
          Search
        </div>
        <div className="relative">
          <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg
              className="w-5 h-5"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <input
            type="text"
            id="table-search"
            className="block p-2 pl-10 w-80 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"
            placeholder="Search for voucher code"
            onChange={(e) => {
              onSearchChange(e.target.value);
            }}
          />
        </div>
      </div>

      <table className="w-full text-sm text-left border-2 border-grey-500">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50">
          <tr>
            <th scope="col" className="py-3 px-6">
              Voucher ID
            </th>
            <th scope="col" className="py-3 px-6">
              Voucher Code
            </th>
            <th scope="col" className="py-3 px-6">
              Discount Percentage
            </th>
            <th scope="col" className="py-3 px-6">
              Discount Limit
            </th>
            <th scope="col" className="py-3 px-6">
              Quota
            </th>
            <th scope="col" className="py-3 px-6">
              Active
            </th>
            <th scope="col" className="py-3 px-6">
              Start Time
            </th>
            <th scope="col" className="py-3 px-6">
              Expire Time
            </th>
          </tr>
        </thead>
        <tbody>{renderData()}</tbody>
      </table>

      <div className="mt-2 flex justify-center">
        <PageSelect
          count={pageCount}
          currentPage={filter.page}
          onPageChange={onPageChange}
        />
      </div>
    </div>
  );
};

VoucherTable.propTypes = {
  vouchers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      voucher_code: PropTypes.string,
      discount_rate: PropTypes.number,
      limit: PropTypes.number,
      quota: PropTypes.number,
      is_active: PropTypes.bool,
      active_at: PropTypes.string,
      expires_at: PropTypes.string,
    }),
  ).isRequired,

  isLoading: PropTypes.bool.isRequired,
  pageCount: PropTypes.number.isRequired,
  onFilterChanged: PropTypes.func.isRequired,
};

export default VoucherTable;
