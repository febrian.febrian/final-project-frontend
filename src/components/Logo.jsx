import React from 'react';
import { Link } from 'react-router-dom';

const Logo = ({ redirectTo }) => {
  return (
    <Link to={redirectTo} className="text-3xl font-bold underline text-white">
      P2P
    </Link>
  );
};

export default Logo;
