import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const apiBaseURL = process.env.REACT_APP_API_BASE_URL;

const transformResponse = (response) => {
  const { error } = response.data;
  if (error) {
    return error;
  }

  return response.data;
};

export const apiSlice = createApi({
  reducerPath: 'p2p-lending-web',
  baseQuery: fetchBaseQuery({
    baseUrl: apiBaseURL,
  }),
  tagTypes: ['user_detail'],
  endpoints: (builder) => {
    return {
      login: builder.mutation({
        query: (payload) => {
          return {
            url: '/login',
            method: 'POST',
            body: JSON.stringify(payload),
          };
        },
        transformResponse: transformResponse,
        invalidatesTags: ['user_profile'],
      }),
      register: builder.mutation({
        query: (payload) => {
          return {
            url: '/register',
            method: 'POST',
            body: JSON.stringify({
              name: payload.name,
              email: payload.email,
              password: payload.password,
            }),
          };
        },
        transformResponse: transformResponse,
      }),
      getUserByID: builder.query({
        query: (userID) => {
          const token = localStorage.getItem('token');
          return {
            url: `/admin/users/${userID}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        transformResponse: transformResponse,
        providesTags: ['user_detail'],
      }),
      updateContractStatus: builder.mutation({
        query: ({ userID, state }) => {
          console.log(userID, state);
          const token = localStorage.getItem('token');
          return {
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
            url: `/admin/users/${userID}/contract`,
            method: 'PATCH',
            body: JSON.stringify({ state }),
          };
        },
        invalidatesTags: ['user_detail', 'user_profile'],
      }),

      updateCreditLimit: builder.mutation({
        query: ({ userID, creditLimit }) => {
          return {
            url: `/admin/users/${userID}/limit`,
            method: 'PATCH',
            body: JSON.stringify({ credit_limit: creditLimit }),
            headers: new Headers({
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            }),
          };
        },
        invalidatesTags: ['user_detail', 'user_profile'],
      }),

      getPendingLoans: builder.query({
        query: ({ search, sortBy, sortDirection, limit, page }) => {
          const queryParameters = `?status=pending&search=${search}&sort=${sortDirection}&sort_by=${sortBy}&limit=${limit}&page=${page}`;

          return {
            url: `/admin/loans${queryParameters}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            }),
          };
        },
        providesTags: ['pending_loans'],
        transformResponse: transformResponse,
      }),

      getActiveLoans: builder.query({
        query: ({ search, sortBy, sortDirection, limit, page }) => {
          const queryParameters = `?status=active&search=${search}&sort=${sortDirection}&sort_by=${sortBy}&limit=${limit}&page=${page}`;

          return {
            url: `/admin/loans${queryParameters}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            }),
          };
        },
        providesTags: ['active_loans'],
        transformResponse: transformResponse,
      }),

      getHistoryLoans: builder.query({
        query: ({ search, sortBy, sortDirection, limit, page }) => {
          const queryParameters = `?status=done&search=${search}&sort=${sortDirection}&sort_by=${sortBy}&limit=${limit}&page=${page}`;

          return {
            url: `/admin/loans${queryParameters}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            }),
          };
        },
        providesTags: ['history_loans'],
        transformResponse: transformResponse,
      }),

      getAuthenticatedUser: builder.query({
        query: () => {
          const token = localStorage.getItem('token');

          return {
            url: '/auth/user',
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        transformResponse: transformResponse,
      }),

      getLoanDetail: builder.query({
        query: (id) => {
          const token = localStorage.getItem('token');

          return {
            url: `/loans/${id}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        transformResponse: transformResponse,
        providesTags: ['loan_detail'],
      }),
      approveLoan: builder.mutation({
        query: (id) => {
          const token = localStorage.getItem('token');

          return {
            url: `/admin/loans/${id}/approve`,
            method: 'PATCH',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        transformResponse: transformResponse,
        invalidatesTags: [
          'loan_detail',
          'pending_loans',
          'active_loans',
          'user_profile',
          'user_detail',
        ],
      }),
      rejectLoan: builder.mutation({
        query: (id) => {
          const token = localStorage.getItem('token');

          return {
            url: `/admin/loans/${id}/reject`,
            method: 'PATCH',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        invalidatesTags: [
          'loan_detail',
          'pending_loans',
          'active_loans',
          'user_profile',
          'user_detail',
        ],
      }),
      changeMonthlyDueDate: builder.mutation({
        query: ({ id, date }) => {
          console.log(id, date);
          const token = localStorage.getItem('token');

          return {
            url: `/admin/loans/${id}/date`,
            method: 'PATCH',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
            body: JSON.stringify({
              day: date,
            }),
          };
        },
        invalidatesTags: ['loan_detail'],
      }),
      getVouchers: builder.query({
        query: ({ search, limit, page, sortBy, sortDirection }) => {
          const token = localStorage.getItem('token');

          return {
            url: `/admin/vouchers?search=${search}&limit=${limit}&page=${page}&sortBy=${sortBy}&sort=${sortDirection}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        providesTags: ['vouchers'],
      }),
      updateVoucherActiveStatus: builder.mutation({
        query: ({ id, status }) => {
          const token = localStorage.getItem('token');

          return {
            url: `/admin/vouchers/${id}/active`,
            method: 'PATCH',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
            body: JSON.stringify({
              status: status,
            }),
          };
        },
        invalidatesTags: ['vouchers'],
      }),
      createVoucher: builder.mutation({
        query: (payload) => {
          const token = localStorage.getItem('token');

          return {
            url: '/admin/vouchers',
            method: 'POST',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
            body: JSON.stringify({
              voucher_code: payload.voucherCode,
              discount_percentage: payload.discountPercentage,
              limit: payload.limit,
              quota: payload.quota,
              active_at: payload.startAt,
              expires_at: payload.expireAt,
            }),
          };
        },
        invalidatesTags: ['vouchers'],
      }),
      getPayments: builder.query({
        query: ({ show, page, limit, search, sortDirection, sortBy }) => {
          const token = localStorage.getItem('token');

          return {
            url: `/admin/payments?search=${search}&page=${page}&limit=${limit}&last=${show}&sort=${sortDirection}&sort_by=${sortBy}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        providesTags: ['vouchers'],
        transformResponse: transformResponse,
      }),
      getProfile: builder.query({
        query: () => {
          const token = localStorage.getItem('token');

          return {
            url: '/users/profile',
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        providesTags: ['user_profile'],
        transformResponse: transformResponse,
      }),

      confirmContract: builder.mutation({
        query: () => {
          const token = localStorage.getItem('token');

          return {
            url: '/users/contract/confirm',
            method: 'PATCH',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        invalidatesTags: ['user_profile', 'user_detail'],
        transformResponse: transformResponse,
      }),
      requestLoan: builder.mutation({
        query: (payload) => {
          const token = localStorage.getItem('token');

          return {
            url: '/loans',
            method: 'POST',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
            body: JSON.stringify({
              purpose: payload.purpose,
              description: payload.description,
              amount: payload.amount,
              tenor: payload.tenor,
            }),
          };
        },
        invalidatesTags: ['user_profile', 'user_detail'],
      }),
      payInstallment: builder.mutation({
        query: (payload) => {
          const token = localStorage.getItem('token');

          return {
            url: '/pay',
            method: 'POST',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
            body: JSON.stringify({
              installment_id: payload.installmentID,
              amount: payload.amount,
              voucher_code: payload.voucherCode,
            }),
          };
        },
        invalidatesTags: ['user_profile', 'user_detail'],
      }),
      getActiveVouchers: builder.query({
        query: () => {
          const token = localStorage.getItem('token');

          return {
            url: '/vouchers',
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        invalidatesTags: ['user_profile'],
        transformResponse,
      }),
      getUsers: builder.query({
        query: ({ contract, sortDirection, sortBy, limit, page, search }) => {
          const token = localStorage.getItem('token');

          return {
            url: `/admin/users?contract=${contract}&limit=${limit}&page=${page}&search=${search}&sort=${sortDirection}&sort_by=${sortBy}`,
            method: 'GET',
            headers: new Headers({
              Authorization: `Bearer ${token}`,
            }),
          };
        },
        providesTags: ['users'],
        transformResponse,
      }),
    };
  },
});

export const {
  useLoginMutation,
  useRegisterMutation,
  useGetUserByIDQuery,
  useUpdateContractStatusMutation,
  useUpdateCreditLimitMutation,

  useLazyGetActiveLoansQuery,
  useLazyGetPendingLoansQuery,
  useLazyGetHistoryLoansQuery,

  useGetAuthenticatedUserQuery,
  useGetLoanDetailQuery,
  useApproveLoanMutation,
  useRejectLoanMutation,
  useChangeMonthlyDueDateMutation,

  useLazyGetUsersQuery,

  useLazyGetVouchersQuery,
  useUpdateVoucherActiveStatusMutation,
  useCreateVoucherMutation,

  useLazyGetPaymentsQuery,

  useGetProfileQuery,
  useConfirmContractMutation,

  useRequestLoanMutation,
  usePayInstallmentMutation,
  useGetActiveVouchersQuery,
} = apiSlice;
