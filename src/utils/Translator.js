const commonResponseMap = {
  80: {
    title: 'Internal error',
  },
  90: {
    title: 'Internal error',
  },
  10: {
    title: 'Unauthenticated',
    message: 'You are unauthenticated',
  },
  11: {
    title: 'Unauthenticated',
    message: 'You are unauthenticated',
  },
  12: {
    title: 'Unauthenticated',
    message: 'You are unauthenticated',
  },
  20: {
    title: 'Forbidden',
    message: 'You are forbidden to access this resource',
  },
  999: {
    title: 'Internal error',
    message: 'Unexpected error occured',
  },
};

export const getUserMap = {
  210: {
    title: 'Error getting user detail',
    message: 'User is not found',
  },
};

export const updateContractStatusMap = {
  111: {
    title: 'Validation error',
    message: 'A contract status is required and not zero',
  },
  112: {
    title: 'Validation error',
    message: 'Invalid contract status value (must be between 1 and 5)',
  },
  113: {
    title: 'Validation error',
    message: 'Invalid contract status value (must be between 1 and 5)',
  },
  210: {
    title: 'User not found',
    message: 'User is not found',
  },
};

export const updateCreditLimitMap = {
  111: {
    title: 'Validation error',
    message: 'A credit limit value is required and not zero',
  },
  112: {
    title: 'Validation error',
    message: 'Invalid credit limit (must be more than 1.000.000)',
  },
  210: {
    title: 'Error updating credit limit',
    message: 'User is not found',
  },
  220: {
    title: 'Error updating credit limit',
    message: 'User credit status is not found',
  },
};

export const registerMap = {
  111: {
    title: 'Register failed',
    message: 'Name is empty',
  },
  121: {
    title: 'Register failed',
    message: 'Email is empty',
  },
  131: {
    title: 'Register failed',
    message: 'Password is empty',
  },
  210: {
    title: 'Register failed',
    message: 'Email is already used',
  },
};

export const loginMap = {
  111: {
    title: 'Login failed',
    message: 'Email is empty',
  },
  121: {
    title: 'Login failed',
    message: 'Password is empty',
  },
  210: {
    title: 'Login failed',
    message: 'Email is not in our database',
  },
  220: {
    title: 'Login failed',
    message: 'Invalid email or password',
  },
};

export const approveLoanMap = {
  210: {
    title: 'Approve loan failed',
    message: 'user not found',
  },
  211: {
    title: 'Approve loan failed',
    message: 'user is not admin',
  },
  220: {
    title: 'Approve loan failed',
    message: 'loan not found',
  },
  221: {
    title: 'Approve loan failed',
    message: 'loan is already approved',
  },
};

export const changeLoanDueDateMap = {
  111: {
    title: 'Update loan due date failed',
    message: 'Bad request',
  },
  210: {
    title: 'Update loan due date failed',
    message: 'loan not found',
  },
};

export const createVoucherMap = {
  154: {
    title: 'Create voucher failed',
    message: 'Invalid start time',
  },
  155: {
    title: 'Create voucher failed',
    message: 'Start time cannot be before current time',
  },
  164: {
    title: 'Create voucher failed',
    message: 'Invalid expire time',
  },
  165: {
    title: 'Create voucher failed',
    message: 'Expire time cannot be before start time',
  },
  210: {
    title: 'Create voucher failed',
    message: 'Voucher code already exists',
  },
};

export const getProfileMap = {
  210: {
    title: 'Get profile failed',
    message: 'Something unexpected occured (user not found)',
  },
};

export const requestLoanMap = {
  210: {
    title: 'Request loan failed',
    message: 'Something unexpected occured (user not found)',
  },
  211: {
    title: 'Request loan failed',
    message: 'You are currently ineligible to request for a loan',
  },
  220: {
    title: 'Request loan failed',
    message: "You haven't signed a contract",
  },
  230: {
    title: 'Request loan failed',
    message: 'Cannot request a loan with this amount (beyond total limit)',
  },
};

export const payInstallmentMap = {
  210: {
    title: 'Pay installment failed',
    message: 'Something unexpected occured (installment id not found)',
  },
  211: {
    title: 'Pay installment failed',
    message: 'Insufficient amount',
  },
  212: {
    title: 'Pay installment failed',
    message: 'Installment is already paid',
  },
  220: {
    title: 'Pay installment failed',
    message: 'Voucher not found',
  },
  221: {
    title: 'Pay installment failed',
    message: 'Voucher is out of quota',
  },
};

export const rejectLoanMap = {
  // RejectLoan_UserNotFound     = 210
  // RejectLoan_UserNotAdmin     = 211
  // RejectLoan_LoanNotFound     = 220
  // RejectLoan_LoanIsNotPending = 221
  210: {
    title: 'Reject loan failed',
    message: 'Something unexpected occured (user not found)',
  },
  211: {
    title: 'Reject loan failed',
    message: 'You do not have administrator role',
  },
  220: {
    title: 'Reject loan failed',
    message: 'Loan is not found',
  },
  221: {
    title: 'Reject loan failed',
    message: 'Cannot reject this loan. Loan status is not pending',
  },
};

export default (endpointMap, errorCode) => {
  const combinedMap = {
    ...commonResponseMap,
    ...endpointMap,
  };

  return combinedMap[errorCode];
};
