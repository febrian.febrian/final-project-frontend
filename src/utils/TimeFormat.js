const addLeadingZero = (n) => {
  if (n < 10) {
    return `0${n}`;
  }
  return n;
};

export const FormatDate = (dateTimeStr) => {
  const dateTime = new Date(dateTimeStr);

  const month = addLeadingZero(dateTime.getMonth() + 1);
  const day = addLeadingZero(dateTime.getDate());

  return `${dateTime.getFullYear()}-${month}-${day}`;
};

export const FormatTime = (dateTimeStr) => {
  const dateTime = new Date(dateTimeStr);

  const hour = addLeadingZero(dateTime.getHours());
  const minute = addLeadingZero(dateTime.getMinutes());
  const second = addLeadingZero(dateTime.getSeconds());

  return `${hour}:${minute}:${second}`;
};

export const FormatDateTime = (dateTimeStr) => {
  return `${FormatDate(dateTimeStr)} ${FormatTime(dateTimeStr)}`;
};
