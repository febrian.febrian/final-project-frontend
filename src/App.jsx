import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Container from './components/Container';
import Navbar from './components/Navbar';
import Login from './pages/Login';
import Register from './pages/Register';
import UserDetail from './pages/UserDetail';
import AdminHome from './pages/Admin/Home';
import AdminGuard from './components/guard/AdminGuard';
import UserGuard from './components/guard/UserGuard';
import GuestGuard from './components/guard/GuestGuard';
import LoanDetail from './pages/LoanDetail';
import Home from './pages/User/Home';

const App = () => {
  return (
    <Container width="w-full">
      <Navbar />
      <Routes>
        <Route
          path="/register"
          element={
            <GuestGuard>
              <Register />
            </GuestGuard>
          }
        />
        <Route
          path="/login"
          element={
            <GuestGuard>
              <Login />
            </GuestGuard>
          }
        />

        <Route
          path="/admin"
          element={
            <AdminGuard>
              <AdminHome />
            </AdminGuard>
          }
        />

        <Route
          path="/admin/loans/:loanID"
          element={
            <AdminGuard>
              <LoanDetail />
            </AdminGuard>
          }
        />

        <Route
          path="/admin/users/:userID"
          element={
            <AdminGuard>
              <UserDetail />
            </AdminGuard>
          }
        />

        <Route
          path="/"
          element={
            <UserGuard>
              <Home />
            </UserGuard>
          }
        />
      </Routes>
    </Container>
  );
};

export default App;
